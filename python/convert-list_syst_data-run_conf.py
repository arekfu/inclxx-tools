#!/usr/bin/env python

import sys

if len(sys.argv)!=6:
    print "Syntax: convert-list_syst-run-conf.py <generator> <deexcitation> <ddxs-linx> <ddxs-logx> <isot>"
    sys.exit()

generator = sys.argv[1]
deexcitation = sys.argv[2]
ddxslinx_file = sys.argv[3]
ddxslogx_file = sys.argv[4]
isot_file = sys.argv[5]

### Real script starts here

shot_dictionary = {}
ddxslinx_dictionary = {}
ddxslogx_dictionary = {}
isot_set = set()

def suggested_nshots(projectile, energy, mass, charge, aej, zej):
    iaej=int(str(aej))
    if iaej==1:
        return 300000
    elif iaej==0:
        return 2500000
    elif iaej<5:
        return 500000
    else:
        return 1000000

def construct_system_name(splitted):
    target = '-'.join(splitted[2:4])
    system = ('_'.join(splitted[:2])) + '_' + target
    return system

def add_system_shot(splitted):
    if len(splitted)>4:
        nshots = suggested_nshots(*splitted)
    else:
        nshots = 500000
    system = construct_system_name(splitted)
    if system in shot_dictionary:
        shot_dictionary[system] = max(nshots, shot_dictionary[system])
    else:
        shot_dictionary[system] = nshots
    return system

def add_system_ddxslinx(line):
    splitted = line.split()
    system = add_system_shot(splitted)
    ejectile = '#'.join(splitted[4:])
    if system in ddxslinx_dictionary:
        ddxslinx_dictionary[system].append(ejectile)
    else:
        ddxslinx_dictionary[system] = [ejectile]

def add_system_ddxslogx(line):
    splitted = line.split()
    system = add_system_shot(splitted)
    ejectile = '#'.join(splitted[4:])
    if system in ddxslogx_dictionary:
        ddxslogx_dictionary[system].append(ejectile)
    else:
        ddxslogx_dictionary[system] = [ejectile]

def add_system_isot(line):
    splitted = line.split()
    system = add_system_shot(splitted)
    isot_set.add(system)

def construct_run_name(s):
    run_name = s
    if s in ddxslinx_dictionary:
        run_name += '_ddxslinx'
        for ej in ddxslinx_dictionary[s]:
            run_name += '#' + ej
    if s in ddxslogx_dictionary:
        run_name += '_ddxslogx'
        for ej in ddxslogx_dictionary[s]:
            run_name += '#' + ej
    if s in isot_set:
        run_name += '_isot'
    return run_name

import runlines



# process the ddxs.linx file
if ddxslinx_file != '':
    with open(ddxslinx_file) as f:
        for line in f:
            add_system_ddxslinx(line)

# process the ddxs.logx file
if ddxslogx_file != '':
    with open(ddxslogx_file) as f:
        for line in f:
            add_system_ddxslogx(line)

# process the ddxs.isot file
if isot_file != '':
    with open(isot_file) as f:
        for line in f:
            add_system_isot(line)


# output the run.conf file
for s, nshots in shot_dictionary.items():
    run_name = construct_run_name(s)
    args = s.split('_')
    args.extend([deexcitation, nshots, generator, run_name])
    print runlines.generate(*args)

