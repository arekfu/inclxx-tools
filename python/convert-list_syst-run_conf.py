#!/usr/bin/env python

import sys

if len(sys.argv)<4:
    print "Syntax: convert-list_syst-run-conf.py <generator> <deexcitation> <input-filename>"
    sys.exit()

generator = sys.argv[1]
deexcitation = sys.argv[2]

import runlines
import fileinput

for line in fileinput.input(sys.argv[3:]):
    projectile, energy, mass, charge, nshots = line.split()
    print runlines.generate(projectile, float(energy), mass + "-" + charge, deexcitation, int(nshots), generator)
