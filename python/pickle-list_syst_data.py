#!/usr/bin/env python

import argparse
import runlines
import cPickle

def suggested_nshots(projectile, energy, mass, charge, aej, zej):
    iaej=int(str(aej))
    if iaej==1:
        return 1000000
    elif iaej==0:
        return 3000000
    elif iaej<5:
        return 2000000
    else:
        return 5000000

def construct_system_name(splitted):
    target = '-'.join(splitted[2:4])
    system = ('_'.join(splitted[:2])) + '_' + target
    return system

def add_system_shot(splitted, n):
    if n==0:
        if len(splitted)>4:
            nshots = suggested_nshots(*splitted)
        else:
            nshots = 500000
    else:
        nshots = n
    system = construct_system_name(splitted)
    if system in shot_dictionary:
        shot_dictionary[system] = max(nshots, shot_dictionary[system])
    else:
        shot_dictionary[system] = nshots
    return system

def add_system_ddxslinx(line):
    splitted = line.split()
    if len(splitted)>6:
        nshots = int(splitted[6])
    else:
        nshots = 0
    if len(splitted)>7:
        minangle = int(splitted[7])
    else:
        minangle = -1.
    if len(splitted)>8:
        maxangle = int(splitted[8])
    else:
        maxangle = 181.
    system = add_system_shot(splitted[:6],nshots)
    ejectile = '#'.join(splitted[4:6])
    if system in ddxslinx_dictionary:
        ddxslinx_dictionary[system].append((ejectile,minangle,maxangle))
    else:
        ddxslinx_dictionary[system] = [(ejectile,minangle,maxangle)]

def add_system_ddxslogx(line):
    splitted = line.split()
    if len(splitted)>6:
        nshots = int(splitted[6])
    else:
        nshots = 0
    if len(splitted)>7:
        minangle = int(splitted[7])
    else:
        minangle = -1.
    if len(splitted)>8:
        maxangle = int(splitted[8])
    else:
        maxangle = 181.
    system = add_system_shot(splitted[:6],nshots)
    ejectile = '#'.join(splitted[4:6])
    if system in ddxslogx_dictionary:
        ddxslogx_dictionary[system].append((ejectile,minangle,maxangle))
    else:
        ddxslogx_dictionary[system] = [(ejectile,minangle,maxangle)]

def add_system_isot(line):
    splitted = line.split()
    if len(splitted)>4:
        nshots = int(splitted[4])
    else:
        nshots = 0
    system = add_system_shot(splitted[:4],nshots)
    isot_set.add(system)

def add_system_ddxspt(line):
    splitted = line.split()
    if len(splitted)>6:
        nshots = int(splitted[6])
    else:
        nshots = 0
    if len(splitted)>7:
        minangle = int(splitted[7])
    else:
        minangle = -1.
    if len(splitted)>8:
        maxangle = int(splitted[8])
    else:
        maxangle = 181.
    system = add_system_shot(splitted[:6],nshots)
    ejectile = '#'.join(splitted[4:6])
    if system in ddxspt_dictionary:
        ddxspt_dictionary[system].append((ejectile,minangle,maxangle))
    else:
        ddxspt_dictionary[system] = [(ejectile,minangle,maxangle)]


### Real script starts here

parser = argparse.ArgumentParser()
parser.add_argument("version", help="name of the version to use")
parser.add_argument("label", help="label for the run")
parser.add_argument("--ddxslinx", help="file containing a list of DDXS systems (lin scale)")
parser.add_argument("--ddxslogx", help="file containing a list of DDXS systems (log scale)")
parser.add_argument("--ddxspt", help="file containing a list of DDXSpT systems")
parser.add_argument("--isot", help="file containing a list of isot systems")
args = parser.parse_args()

version = args.version
output_label = args.label

shot_dictionary = {}
ddxslinx_dictionary = {}
ddxslogx_dictionary = {}
ddxspt_dictionary = {}
isot_set = set()

# process the ddxs.linx file
if args.ddxslinx:
    with open(args.ddxslinx) as f:
        for line in f:
            add_system_ddxslinx(line)

# process the ddxs.logx file
if args.ddxslogx:
    with open(args.ddxslogx) as f:
        for line in f:
            add_system_ddxslogx(line)

# process the ddxspt file
if args.ddxspt:
    with open(args.ddxspt) as f:
        for line in f:
            add_system_ddxspt(line)

# process the ddxs.isot file
if args.isot:
    with open(args.isot) as f:
        for line in f:
            add_system_isot(line)

# output the run.conf file
run_conf_out = 'run-' + output_label + '-' + version + '.conf'
with open(run_conf_out, 'w') as f:
    for s, nshots in shot_dictionary.items():
        if 'G4MT' in version:
            nshots = int(nshots/3)
        args = s.split('_')
        args.extend([nshots, version, s])
        line = runlines.generate(*args)
        if line:
            f.write(line)

# output the pickled files
pickled_out = 'pickled-' + output_label + '-' + version + '.dat'
with open(pickled_out, 'w') as f:
     cPickle.dump(ddxslinx_dictionary, f, cPickle.HIGHEST_PROTOCOL)
     cPickle.dump(ddxslogx_dictionary, f, cPickle.HIGHEST_PROTOCOL)
     cPickle.dump(ddxspt_dictionary, f, cPickle.HIGHEST_PROTOCOL)
     cPickle.dump(isot_set, f, cPickle.HIGHEST_PROTOCOL)

