import numpy # for the energy scans
import string
import re

# Dictionary for element names
element_dictionary = {
    1: "H",
    2: "He",
    3: "Li",
    4: "Be",
    5: "B",
    6: "C",
    7: "N",
    8: "O",
    9: "F",
    10: "Ne",
    11: "Na",
    12: "Mg",
    13: "Al",
    14: "Si",
    15: "P",
    16: "S",
    17: "Cl",
    18: "Ar",
    19: "K",
    20: "Ca",
    21: "Sc",
    22: "Ti",
    23: "V",
    24: "Cr",
    25: "Mn",
    26: "Fe",
    27: "Co",
    28: "Ni",
    29: "Cu",
    30: "Zn",
    31: "Ga",
    32: "Ge",
    33: "As",
    34: "Se",
    35: "Br",
    36: "Kr",
    37: "Rb",
    38: "Sr",
    39: "Y",
    40: "Zr",
    41: "Nb",
    42: "Mo",
    43: "Tc",
    44: "Ru",
    45: "Rh",
    46: "Pd",
    47: "Ag",
    48: "Cd",
    49: "In",
    50: "Sn",
    51: "Sb",
    52: "Te",
    53: "I",
    54: "Xe",
    55: "Cs",
    56: "Ba",
    57: "La",
    58: "Ce",
    59: "Pr",
    60: "Nd",
    61: "Pm",
    62: "Sm",
    63: "Eu",
    64: "Gd",
    65: "Tb",
    66: "Dy",
    67: "Ho",
    68: "Er",
    69: "Tm",
    70: "Yb",
    71: "Lu",
    72: "Hf",
    73: "Ta",
    74: "W",
    75: "Re",
    76: "Os",
    77: "Ir",
    78: "Pt",
    79: "Au",
    80: "Hg",
    81: "Tl",
    82: "Pb",
    83: "Bi",
    84: "Po",
    85: "At",
    86: "Rn",
    87: "Fr",
    88: "Ra",
    89: "Ac",
    90: "Th",
    91: "Pa",
    92: "U",
    93: "Np",
    94: "Pu",
    95: "Am",
    96: "Cm",
    97: "Bk",
    98: "Cf",
    99: "Es",
    100: "Fm",
    101: "Md",
    102: "No",
    103: "Lr",
    104: "Rf",
    105: "Db",
    106: "Sg",
    107: "Bh",
    108: "Hs",
    109: "Mt",
    110: "Ds",
    111: "Rg",
    112: "Cn"
}

element_to_z_dictionary = dict((v,k) for k,v in element_dictionary.items())

# Dictionary for projectile types
projectile_dictionary = {
    'p': 1,
    'n': 2,
    'pi+': 3,
    'pip': 3,
    'pi0': 4,
    'pi-': 5,
    'pim': 5,
    'd': 6,
    'h2': 6,
    '2h': 6,
    'h-2': 6,
    '2-h': 6,
    't': 7,
    'h3': 7,
    '3h': 7,
    'h-3': 7,
    '3-H': 7,
    'he3': 8,
    '3he': 8,
    'he-3': 8,
    '3-he': 8,
    'a': 9,
    'he4': 9,
    '4he': 9,
    'he-4': 9,
    '4-he': 9
}
# Dictionary for particle types
particle_dictionary = {
    'p': (1,1),
    'n': (1,0),
    'pi+': (0,1),
    'pip': (0,1),
    'piplus': (0,1),
    'pi0': (0,0),
    'pin': (0,0),
    'pizero': (0,0),
    'piz': (0,0),
    'pi-': (0,-1),
    'pim': (0,-1),
    'piminus': (0,-1),
    'd': (2,1),
    't': (3,1),
    'a': (4,2)
}

# Dictionary for de-excitation codes for SMoP
de_excitation_dictionary = {
    'none': 0,
    'abla': 1,
    'abla07': 1,
    'gem': 2,
    'smm': 3,
    'dresner': 4,
    'gemini': 5,
    'geminixx': 5,
    'gemini++': 5
}

def array_of_energies(emin, emax, nenergies, log):
    if 'log' in log.lower():
        return numpy.logspace(numpy.log10(emin), numpy.log10(emax), nenergies)
    else:
        return numpy.linspace(emin, emax, nenergies)

def generate(projectile, energy, target, nshots, generator, run_name=''):
    generator_lower = generator.lower()
    if 'smop' in generator_lower:
        match = re.search('-(\w+)$', generator_lower)
        de_excitation = match.group(1)
        line = generate_smop(projectile, energy, target, nshots, run_name, de_excitation)
    elif 'inclxx-az' in generator_lower:
        line = generate_v5_0(projectile, energy, target, nshots, run_name)
    else:
        line = generate_inclxx(projectile, energy, target, nshots, run_name)
    if line:
        return line + '\n'
    else:
        return None

def generate_inclxx(projectile, energy, target, nshots, run_name=''):
    [ mass, charge ] = target.split('-')
    name = element_dictionary[int(charge)]
    if int(mass)==0:
      tname = name
    else:
      tname = name + mass
    energy_normalised = "{:.2f}".format(float(str(energy)))
    if run_name=='':
        run_name = projectile + "_" + str(energy_normalised) + "_" + str(target)
    line =  run_name \
        + " -p" + projectile \
        + " -t" + tname \
        + " -E" + str(energy) \
        + " -N" + str(nshots) \
        + " -o " + run_name
    return line

def generate_smop(projectile, energy, target, nshots, run_name='', de_excitation=''):
    if not projectile.lower() in projectile_dictionary:
        return None
    projectile_string = str(projectile_dictionary[projectile.lower()])
    if not de_excitation.lower() in de_excitation_dictionary:
        return None
    de_excitation_code = de_excitation_dictionary[de_excitation.lower()]
    [ mass, charge ] = target.split('-')
    energy_normalised = "{:.2f}".format(float(str(energy)))
    if run_name=='':
        run_name = projectile + "_" + str(energy_normalised) + "_" + str(target)
    line = run_name \
        + " " + projectile_string \
        + " " + mass \
        + " " + charge \
        + " " + str(energy) \
        + " " + str(nshots) \
        + " " + str(de_excitation_code)
    return line

def generate_v5_0(projectile, energy, target, nshots, run_name=''):
    [ mass, charge ] = target.split('-')
    energy_normalised = "{:.2f}".format(float(str(energy)))
    if run_name=='':
        run_name = projectile + "_" + str(energy_normalised) + "_" + str(target)
    line = run_name \
        + " -p" + projectile \
        + " -A" + mass \
        + " -Z" + charge \
        + " -E" + str(energy) \
        + " -N" + str(nshots) \
        + " -o " + run_name
    return line

def parse_nuclide(nuclide):
    if any((d in nuclide) for d in string.digits):
        if nuclide[0].isdigit():
            regex = '(\d+)-*([a-zA-Z]+)'
            matches = re.search(regex, nuclide)
            element = matches.group(2)
            mass = int(matches.group(1))
        else:
            regex = '([a-zA-Z]+)-*(\d+)'
            matches = re.search(regex, nuclide)
            element = matches.group(1)
            mass = int(matches.group(2))
    else:
        element = nuclide
        mass = 0
    charge = element_to_z_dictionary[element.capitalize()]
    return mass, charge

def parse_particle(particle):
    if particle in particle_dictionary:
        return particle_dictionary[particle]
    else:
        return parse_nuclide(particle)
