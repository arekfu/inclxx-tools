#!/bin/bash

HASH=$(git rev-parse HEAD)
TAGS=$(git name-rev --name-only --tags ${HASH})
HASH_SHORT=$(echo -n ${HASH} | cut -c -8)

HASH_SHORT_IN_TAGS=$(cat src/tags | sed -e 's/\(.*\)_\([0-9a-f]*\)$/\2/g')
TAGS_IN_TAGS=$(cat src/tags | sed -e 's/\(.*\)_\([0-9a-f]*\)$/\1/g')

git update-index -q --refresh
TOREFRESH=$(git diff-index --name-only HEAD --)

if [ x${HASH_SHORT} != x${HASH_SHORT_IN_TAGS} -o x${TAGS} != x${TAGS_IN_TAGS} -o "x${TOREFRESH}" != x ]; then

	if [ "x$TOREFRESH" != x ]; then
		HASH="$HASH-dirty"
		HASH_SHORT="$HASH_SHORT-dirty"
	fi

	echo "Updating versioning files..."

	echo "      character*80 git_hash, git_tags" > src/MAIN/version.inc
	echo "      parameter( git_hash =" >> src/MAIN/version.inc
	echo "     &'$HASH' )" >> src/MAIN/version.inc
	echo "      parameter( git_tags = '${TAGS}' )" >> src/MAIN/version.inc

	echo -n "${TAGS}_${HASH_SHORT}" | sed -e 's/ /_/g' > src/tags

fi
