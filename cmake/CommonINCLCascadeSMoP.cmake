# Needs Python to generate the list of runs and to post-process
find_package(PythonInterp REQUIRED)



# Function that gives the output dir for a comparison plot
function(get_plot_target_name VAR LABEL1 LABEL2)
  set(TARGET_NAME "plot_${LABEL1}_${LABEL2}")
  set(${VAR} "${TARGET_NAME}" PARENT_SCOPE)
endfunction(get_plot_target_name VAR LABEL1 LABEL2)



# Function that gives the output dir for a comparison plot
function(get_plot_output_dir VAR LABEL1 LABEL2)
  set(OUTPUT_DIR "${PROJECT_BINARY_DIR}/results/plots/${LABEL1}_${LABEL2}")
  file(MAKE_DIRECTORY ${OUTPUT_DIR})
  set(${VAR} "${OUTPUT_DIR}" PARENT_SCOPE)
endfunction(get_plot_output_dir VAR LABEL1 LABEL2)



# Function that gives the output file for a given comparison plot
function(get_plot_output_file VAR LABEL1 LABEL2)
  get_plot_output_dir(OUTPUT_DIR "${LABEL1}" "${LABEL2}")
  set(OUTPUT_FILE "${OUTPUT_DIR}/plot_${LABEL1}_${LABEL2}.pdf")
  set(${VAR} "${OUTPUT_FILE}" PARENT_SCOPE)
endfunction(get_plot_output_file VAR LABEL1 LABEL2)



# Function that generates the list of runs
function(generate_list_of_runs FILE_TO_CONFIGURE SCRIPT_OUTPUT_FILE LABEL)
    set(INCLCASCADE_SCAN_GENERATOR ${LABEL})
    get_filename_component(INCLCASCADE_GENERATE_SCRIPT_NAME "${FILE_TO_CONFIGURE}" NAME)
    string(REGEX REPLACE "^(.*)\\.[^.]+$" "\\1" INCLCASCADE_GENERATE_SCRIPT_NAME ${INCLCASCADE_GENERATE_SCRIPT_NAME})
    set(INCLCASCADE_GENERATE_SCRIPT "${CMAKE_CURRENT_BINARY_DIR}/${LABEL}-${INCLCASCADE_GENERATE_SCRIPT_NAME}")
    configure_file("${FILE_TO_CONFIGURE}" ${INCLCASCADE_GENERATE_SCRIPT} @ONLY)
    execute_process(COMMAND ${PYTHON_EXECUTABLE} ${INCLCASCADE_GENERATE_SCRIPT}
                    OUTPUT_FILE ${SCRIPT_OUTPUT_FILE}
                    )
endfunction(generate_list_of_runs FILE_TO_CONFIGURE OUTPUT_FILE)



# Function that gives the target name for a given run/version combination
function(get_run_target_name VAR RUN_NAME VERSION)
  set(${VAR} "run-${RUN_NAME}-${VERSION}" PARENT_SCOPE)
endfunction(get_run_target_name VAR RUN_NAME VERSION)



# Function that gives the output dir for a given run/version combination
function(get_batch_output_dir VAR LABEL)
  set(OUTPUT_DIR "${PROJECT_BINARY_DIR}/results/${LABEL}")
  file(MAKE_DIRECTORY ${OUTPUT_DIR})
  set(${VAR} ${OUTPUT_DIR} PARENT_SCOPE)
endfunction(get_batch_output_dir VAR LABEL)



# Function that gives the output dir for a given run/version combination
function(get_run_output_dir VAR RUN_NAME INCLCASCADE_VERSION)
  set(OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}/results/${INCLCASCADE_VERSION}/${RUN_NAME}")
  file(MAKE_DIRECTORY ${OUTPUT_DIR})
  set(${VAR} ${OUTPUT_DIR} PARENT_SCOPE)
endfunction(get_run_output_dir VAR RUN_NAME INCLCASCADE_VERSION)



# Function that gives the output file for a time run
function(get_timed_run_output_file VAR RUN_NAME VERSION)
  get_run_output_dir(OUTPUT_DIR ${RUN_NAME} ${VERSION})
  set(${VAR} "${OUTPUT_DIR}/${RUN_NAME}.timing" PARENT_SCOPE)
endfunction(get_timed_run_output_file VAR RUN_NAME VERSION)



# Function that gives the output files for a given SMoP run
function(get_SMoP_run_output_files VAR RUN_NAME VERSION)
  get_SMoP_run_output_HBOOK_files(HBOOK_FILES "${RUN_NAME}" "${VERSION}")
  get_SMoP_run_output_ROOT_file(ROOT_FILE "${RUN_NAME}" "${VERSION}")
  set(${VAR} "${HBOOK_FILES};${ROOT_FILE}" PARENT_SCOPE)
endfunction(get_SMoP_run_output_files VAR RUN_NAME VERSION)



# Function that gives the output files for a given SMoP run
function(get_SMoP_run_output_HBOOK_files VAR RUN_NAME VERSION)
  get_run_output_dir(OUTPUT_DIR ${RUN_NAME} ${VERSION})

  string(TOLOWER "${RUN_NAME}.hbk" LOWER_HBOOK_FILE)
  set(OUTPUT_HBOOK_FILE "${OUTPUT_DIR}/${LOWER_HBOOK_FILE}")
  set(OUTPUT_LOG_FILE "${OUTPUT_DIR}/${RUN_NAME}.log")
  set(OUTPUT_FILES "${OUTPUT_HBOOK_FILE}" "${OUTPUT_LOG_FILE}")
  if(SMOP_RUN_TIMED)
    get_timed_run_output_file(OUTPUT_TIME_FILE ${RUN_NAME} ${VERSION})
    list(APPEND OUTPUT_FILES ${OUTPUT_TIME_FILE})
  endif(SMOP_RUN_TIMED)

  set(${VAR} ${OUTPUT_FILES} PARENT_SCOPE)
endfunction(get_SMoP_run_output_HBOOK_files VAR RUN_NAME VERSION)



# Function that gives the ROOT output file for a given SMoP run
function(get_SMoP_run_output_ROOT_file VAR RUN_NAME VERSION)
  get_run_output_dir(OUTPUT_DIR ${RUN_NAME} ${VERSION})
  set(OUTPUT_ROOT_FILE "${OUTPUT_DIR}/${RUN_NAME}.root")
  set(${VAR} ${OUTPUT_ROOT_FILE} PARENT_SCOPE)
endfunction(get_SMoP_run_output_ROOT_file VAR RUN_NAME VERSION)



# Function that gives the output files for a given run
function(get_G4MT_run_output_files VAR RUN_NAME VERSION)
  get_run_output_dir(OUTPUT_DIR ${RUN_NAME} ${VERSION})

  set(OUTPUT_ROOT_FILE "${OUTPUT_DIR}/${RUN_NAME}.root")
  set(OUTPUT_LOG_FILE "${OUTPUT_DIR}/${RUN_NAME}.log")
  set(OUTPUT_FILES ${OUTPUT_ROOT_FILE} ${OUTPUT_LOG_FILE})
  if(G4MODELTESTER_RUN_TIMED)
    get_timed_run_output_file(OUTPUT_TIME_FILE ${RUN_NAME} ${VERSION})
    list(APPEND OUTPUT_FILES ${OUTPUT_TIME_FILE})
  endif(G4MODELTESTER_RUN_TIMED)

  set(${VAR} ${OUTPUT_FILES} PARENT_SCOPE)
endfunction(get_G4MT_run_output_files VAR RUN_NAME VERSION)



# Function that gives the output files for a given run
function(get_INCLCascade_run_output_files VAR RUN_NAME VERSION)
  get_run_output_dir(OUTPUT_DIR ${RUN_NAME} ${VERSION})

  set(OUTPUT_LOG_FILE "${OUTPUT_DIR}/${RUN_NAME}.log")
  set(OUTPUT_FILES ${OUTPUT_LOG_FILE})
  if(INCLCASCADE_RUN_TIMED)
    get_timed_run_output_file(OUTPUT_TIME_FILE ${RUN_NAME} ${VERSION})
    list(APPEND OUTPUT_FILES ${OUTPUT_TIME_FILE})
  endif(INCLCASCADE_RUN_TIMED)
  if(INCLCASCADE_ROOT_USE)
    set(OUTPUT_ROOT_FILE "${OUTPUT_DIR}/${RUN_NAME}.root")
    list(APPEND OUTPUT_FILES ${OUTPUT_ROOT_FILE})
  endif(INCLCASCADE_ROOT_USE)

  set(${VAR} ${OUTPUT_FILES} PARENT_SCOPE)
endfunction(get_INCLCascade_run_output_files VAR RUN_NAME VERSION)



# Function that gives the output files for a given run
function(get_run_output_files VAR RUN_NAME VERSION)
  string(TOLOWER ${VERSION} LOWER_VERSION)
  if(LOWER_VERSION MATCHES "smop")
    get_SMoP_run_output_files(INTERMEDIATE_VAR ${RUN_NAME} ${VERSION})
  elseif(LOWER_VERSION MATCHES "g4mt")
    get_G4MT_run_output_files(INTERMEDIATE_VAR ${RUN_NAME} ${VERSION})
  else()
    get_INCLCascade_run_output_files(INTERMEDIATE_VAR ${RUN_NAME} ${VERSION})
  endif()

  set(${VAR} ${INTERMEDIATE_VAR} PARENT_SCOPE)
endfunction(get_run_output_files VAR RUN_NAME VERSION GENERATOR)



# Function that gives the output file of a post-processing script for a given run
function(get_script_run_output_file VAR RUN_NAME VERSION)
  get_run_output_dir(OUTPUT_DIR ${RUN_NAME} ${VERSION})
  set(${VAR} "${OUTPUT_DIR}/processed-${RUN_NAME}.dat" PARENT_SCOPE)
endfunction(get_script_run_output_file VAR RUN_NAME VERSION)



# Function that gives the output file of a post-processing script for a batch
function(get_script_batch_output_file VAR LABEL)
  get_batch_output_dir(OUTPUT_DIR ${LABEL})
  set(${VAR} "${OUTPUT_DIR}/processed-${LABEL}.dat" PARENT_SCOPE)
endfunction(get_script_batch_output_file VAR LABEL)



# Function that gives the target name for a given run/version combination
function(get_batch_target_name VAR LABEL)
  set(${VAR} "batch-${LABEL}" PARENT_SCOPE)
endfunction(get_batch_target_name VAR LABEL)



# Function that gives the target name for a given run/version combination
function(get_processed_run_target_name VAR RUN_NAME VERSION)
  get_run_target_name(TARGET_NAME ${RUN_NAME} ${VERSION})
  set(${VAR} "processed-${TARGET_NAME}" PARENT_SCOPE)
endfunction(get_processed_run_target_name VAR RUN_NAME VERSION)



# Function that gives the target name for a given run/version combination
function(get_processed_batch_target_name VAR LABEL)
  get_batch_target_name(TARGET_NAME ${LABEL})
  set(${VAR} "processed-${TARGET_NAME}" PARENT_SCOPE)
endfunction(get_processed_batch_target_name VAR LABEL)



# Function that defines postprocessing for an INCLCascade run
#
# Args passed to the python script: script_output_file run_output_file_1 run_output_file_2 ...
#
function(postprocess_run SCRIPT_TO_CONFIGURE RUN_NAME VERSION OPTIONS)

  # Get the list of the output files for the run
  get_run_output_files(OUTPUT_FILES ${RUN_NAME} ${VERSION})

  # Get the name of the processed output file
  get_script_run_output_file(SCRIPT_OUTPUT_FILE ${RUN_NAME} ${VERSION})

  # Generate the python script
  get_filename_component(SCRIPT_NAME "${SCRIPT_TO_CONFIGURE}" NAME)
  string(REGEX REPLACE "^(.*)\\.[^.]+$" "\\1" SCRIPT_NAME ${SCRIPT_NAME})
  get_run_output_dir(OUTPUT_DIR ${RUN_NAME} ${VERSION})
  set(SCRIPT "${OUTPUT_DIR}/${RUN_NAME}-${SCRIPT_NAME}")
  configure_file("${SCRIPT_TO_CONFIGURE}" ${SCRIPT} @ONLY)

  # How to create the processed file
  add_custom_command(OUTPUT ${SCRIPT_OUTPUT_FILE}
                     COMMAND ${PYTHON_EXECUTABLE} ${SCRIPT} ${SCRIPT_OUTPUT_FILE} ${OUTPUT_FILES}
                     )

  get_run_target_name(TARGET_NAME ${RUN_NAME} ${VERSION})
  get_processed_run_target_name(PROCESSED_TARGET_NAME ${RUN_NAME} ${VERSION})
  add_custom_target(${PROCESSED_TARGET_NAME}
                    ALL
                    DEPENDS ${SCRIPT_OUTPUT_FILE}
                   )

  add_dependencies(${PROCESSED_TARGET_NAME} ${TARGET_NAME})

  message(STATUS "Added target ${PROCESSED_TARGET_NAME}")
  message(STATUS "... will produce the following output file: ${SCRIPT_OUTPUT_FILE}")
  message(STATUS "... depends on the following run target: ${TARGET_NAME}")

endfunction(postprocess_run SCRIPT_TO_CONFIGURE RUN_NAME VERSION OPTIONS)



# Function that defines postprocessing for an INCLCascade run
#
# Args passed to the python script: script_output_file run_output_file_1 run_output_file_2 ...
#
function(postprocess_batch SCRIPT_TO_CONFIGURE RUNS VERSION LABEL)

  # Generate the python script
  get_filename_component(SCRIPT_NAME "${SCRIPT_TO_CONFIGURE}" NAME)
  string(REGEX REPLACE "^(.*)\\.[^.]+$" "\\1" SCRIPT_NAME ${SCRIPT_NAME})
  set(VERSION_LABEL "${VERSION}-${LABEL}")
  get_batch_output_dir(OUTPUT_DIR ${VERSION_LABEL})
  set(SCRIPT "${OUTPUT_DIR}/${VERSION_LABEL}-${SCRIPT_NAME}")
  configure_file("${SCRIPT_TO_CONFIGURE}" ${SCRIPT} @ONLY)

  set(SCRIPT_OUTPUT_FILES "")
  set(SCRIPT_TARGET_NAMES "")
  foreach(RUN_NAME ${RUNS})
    # Get the output files for the post-processing script
    get_script_run_output_file(SCRIPT_OUTPUT_FILE ${RUN_NAME} ${VERSION})
    list(APPEND SCRIPT_OUTPUT_FILES ${SCRIPT_OUTPUT_FILE})
    # Get the target names for the post-processing script
    get_processed_run_target_name(SCRIPT_TARGET_NAME ${RUN_NAME} ${VERSION})
    list(APPEND SCRIPT_TARGET_NAMES ${SCRIPT_TARGET_NAME})
  endforeach(RUN_NAME ${RUNS})

  # How to create the processed file
  get_script_batch_output_file(SCRIPT_BATCH_OUTPUT_FILE ${VERSION_LABEL})
  add_custom_command(OUTPUT ${SCRIPT_BATCH_OUTPUT_FILE}
                     COMMAND ${PYTHON_EXECUTABLE} ${SCRIPT} ${SCRIPT_BATCH_OUTPUT_FILE} ${SCRIPT_OUTPUT_FILES}
                     )

  get_processed_batch_target_name(PROCESSED_TARGET_NAME ${VERSION_LABEL})
  add_custom_target(${PROCESSED_TARGET_NAME}
                    ALL
                    DEPENDS ${SCRIPT_BATCH_OUTPUT_FILE}
                   )

  add_dependencies(${PROCESSED_TARGET_NAME} ${SCRIPT_TARGET_NAMES})

  message(STATUS "Added target ${PROCESSED_TARGET_NAME}")
  message(STATUS "... will produce the following output file: ${SCRIPT_BATCH_OUTPUT_FILE}")
  message(STATUS "... depends on the following run target: ${SCRIPT_TARGET_NAMES}")

endfunction(postprocess_batch SCRIPT_TO_CONFIGURE RUNS VERSION LABEL)



