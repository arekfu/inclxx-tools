cmake_minimum_required(VERSION 2.8)

# Script for parsing function arguments
include(CMakeParseArguments)

# Functions to generate run files from Python scripts, to generate target and directory names...
include(CommonINCLCascadeSMoP)



# Get the directory of this module file
get_filename_component(RUNSMOP_MODULE_DIR ${CMAKE_CURRENT_LIST_FILE} PATH)
message(STATUS "RunSMoP initialised from directory: ${RUNSMOP_MODULE_DIR}")
get_filename_component(TOOLS_DIR ${RUNSMOP_MODULE_DIR} PATH)
message(STATUS " ... will look for cu46.in.in file in: ${TOOLS_DIR}")
set(RUNSMOP_PYTHON_DIR "${TOOLS_DIR}/python")
message(STATUS " ... will look for Python scripts in: ${RUNSMOP_PYTHON_DIR}")

set(SMOP_VERSIONS "" CACHE STRING "List of SMoP versions" FORCE)

# INCLTreeConverter program
find_program(INCLTREECONVERTER_EXECUTABLE INCLTreeConverter)
if(NOT INCLTREECONVERTER_EXECUTABLE)
  message(FATAL_ERROR "You must set the INCLTREECONVERTER_EXECUTABLE variable to the path to the INCLTreeConverter program")
endif(NOT INCLTREECONVERTER_EXECUTABLE)
# Recent versions of INCLTreeConverter can directly handle the HBOOK-to-et
# conversion.  Check if this is the case.
execute_process(COMMAND ${INCLTREECONVERTER_EXECUTABLE} --can-handle-hbook
  RESULT_VARIABLE INCLTREECONVERTER_OPTION_RESULT
                OUTPUT_VARIABLE INCLTREECONVERTER_CAN_HANDLE_HBOOK
                ERROR_QUIET)
if(NOT INCLTREECONVERTER_OPTION_RESULT EQUAL 0)
  set(INCLTREECONVERTER_CAN_HANDLE_HBOOK "False")
endif(NOT INCLTREECONVERTER_OPTION_RESULT EQUAL 0)

if(NOT INCLTREECONVERTER_CAN_HANDLE_HBOOK)
  message(STATUS "INCLTreeConverter cannot handle HBOOK files natively")
  find_program(H2ROOT_EXECUTABLE h2root)
  if(NOT H2ROOT_EXECUTABLE)
    message(FATAL_ERROR "Cannot find h2root program to convert HBOOK files to ROOT format")
  endif(NOT H2ROOT_EXECUTABLE)
else(NOT INCLTREECONVERTER_CAN_HANDLE_HBOOK)
  message(STATUS "INCLTreeConverter can handle HBOOK files natively")
endif(NOT INCLTREECONVERTER_CAN_HANDLE_HBOOK)

####### Functions #######

# Function that adds build targets for a given SMoP version
function(add_SMoP_version _NAME _VERSION)

  list(FIND SMOP_VERSIONS ${_NAME} VERSION_FOUND)
  if(NOT VERSION_FOUND EQUAL "-1")
    message(STATUS "A SMoP version called ${_NAME} already exists. Will not add a new one.")
    return()
  endif(NOT VERSION_FOUND EQUAL "-1")

  message(STATUS "Adding build-SMoP_${_NAME} target...")

  set(SMOP_RUNS_${VERSION_NAME} "" CACHE STRING "List of runs for SMoP version ${VERSION_NAME}" FORCE)

  set(SMOP_BINARY_FILE "${PROJECT_BINARY_DIR}/${_NAME}/smop")
  set(TARGET_NAME "build-SMoP_${_NAME}")

  add_custom_command(OUTPUT "${SMOP_BINARY_FILE}"
                     COMMAND ${CMAKE_COMMAND}
                     -D "SMOP_VERSION:STRING=${_VERSION}"
                     -D "SMOP_NAME:STRING=${_NAME}"
                     -D "SMOP_GIT_REPOSITORY:STRING=${SMOP_GIT_REPOSITORY}"
                     -D "TOOLS_DIR:FILEPATH=${TOOLS_DIR}"
                     -P "${RUNSMOP_MODULE_DIR}/BuildSMoP.cmake"
                     WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
                     )

  add_custom_target(${TARGET_NAME}
                    DEPENDS "${SMOP_BINARY_FILE}"
                    )

  if(SMOP_VERSIONS)
    set(SMOP_VERSIONS "${SMOP_VERSIONS};${_NAME}" CACHE STRING "List of SMoP versions" FORCE)
  else(SMOP_VERSIONS)
    set(SMOP_VERSIONS "${_NAME}" CACHE STRING "List of SMoP versions" FORCE)
  endif(SMOP_VERSIONS)

  set(SMOP_GIT_VERSION_${_NAME} ${_VERSION} CACHE STRING "Git version of version ${_NAME}" FORCE)

  string(TOLOWER ${_NAME} LOWER_NAME)
  if(LOWER_NAME MATCHES "cu46")
    # SMoP requires cu46.in
    set(SMOP_CONFIGURED_INPUT_FILE_${_NAME} "cu46.in" CACHE STRING "Name of the input file for SMoP ${_NAME}" FORCE)
  else(LOWER_NAME MATCHES "cu46")
    # SMoP requires cu47.in
    set(SMOP_CONFIGURED_INPUT_FILE_${_NAME} "cu47.in" CACHE STRING "Name of the input file for SMoP ${_NAME}" FORCE)
  endif(LOWER_NAME MATCHES "cu46")

  message(STATUS "... version ${_NAME}: build-SMoP_${_VERSION} target")
  message(STATUS "... will build ${SMOP_BINARY_FILE}")
  message(STATUS "... will use ${SMOP_CONFIGURED_INPUT_FILE_${_NAME}} as an input file")

endfunction(add_SMoP_version _NAME _VERSION)



# Function that adds build targets for a given SMoP run
function(add_SMoP_run _NAME _VERSION SMOP_PROJECTILE SMOP_TARGETA SMOP_TARGETZ SMOP_ENERGY SMOP_DEEXCITATION SMOP_NSHOTS)

  get_run_target_name(TARGET_NAME ${_NAME} ${_VERSION})

  list(FIND SMOP_RUNS_${_VERSION} ${TARGET_NAME} RUN_FOUND)
  if(NOT RUN_FOUND EQUAL "-1")
    message(STATUS "A SMoP run called ${TARGET_NAME} already exists. Will not add a new one.")
    return()
  endif(NOT RUN_FOUND EQUAL "-1")

  message(STATUS "Adding ${TARGET_NAME} target...")

  set(SMOP_BINARY_FILE "${PROJECT_BINARY_DIR}/${_VERSION}/smop")

  get_SMoP_run_output_HBOOK_files(OUTPUT_FILES ${_NAME} ${_VERSION})
  get_SMoP_run_output_ROOT_file(OUTPUT_ROOT_FILE ${_NAME} ${_VERSION})

  if(SMOP_RUN_TIMED)
    # Look for the time program
    find_program(TIME_EXECUTABLE time)
    if(NOT TIME_EXECUTABLE)
      message(WARNING "Cannot find time executable, timed runs will not be built")
      return()
    endif(NOT TIME_EXECUTABLE)
    set(TIME_OUTPUT_FORMAT "%U %S %e" CACHE STRING "Output format for the time command")

  endif(SMOP_RUN_TIMED)
  set(TARGET_NAME "run-${_NAME}-${_VERSION}")

  get_run_output_dir(OUTPUT_DIR ${_NAME} ${_VERSION})

  if(NOT SMOP_TABLES_DIR)
    set(SMOP_TABLES_DIR "${CMAKE_BINARY_DIR}/${_VERSION}/tables/")
  endif(NOT SMOP_TABLES_DIR)

  set(CONFIGURED_INPUT_FILE ${SMOP_CONFIGURED_INPUT_FILE_${_VERSION}})
  set(SMOP_INPUT_FILE "${OUTPUT_DIR}/${CONFIGURED_INPUT_FILE}")
  list(GET OUTPUT_FILES 0 OUTPUT_HBOOK_FULL_FILENAME)
  list(GET OUTPUT_FILES 1 OUTPUT_LOG_FULL_FILENAME)
  get_filename_component(OUTPUT_HBOOK_FILENAME ${OUTPUT_HBOOK_FULL_FILENAME} NAME)
  get_filename_component(OUTPUT_LOG_FILENAME ${OUTPUT_LOG_FULL_FILENAME} NAME)
  configure_file("${TOOLS_DIR}/${CONFIGURED_INPUT_FILE}.in" "${SMOP_INPUT_FILE}" @ONLY)

  set(OUTPUT_DEPENDENCIES "${OUTPUT_ROOT_FILE}")

  if(SMOP_RUN_TIMED)
    get_timed_run_output_file(OUTPUT_TIME_FILE ${RUN_NAME} ${VERSION})
    set(COMMAND_TO_EXECUTE ${TIME_EXECUTABLE} "-o${OUTPUT_TIME_FILE}" "-f${TIME_OUTPUT_FORMAT}"
                           ${SMOP_BINARY_FILE}
                           )
    list(APPEND OUTPUT_DEPENDENCIES "${OUTPUT_TIME_FILE}")
  else(SMOP_RUN_TIMED)
    set(COMMAND_TO_EXECUTE ${SMOP_BINARY_FILE})
  endif(SMOP_RUN_TIMED)

  add_custom_command(OUTPUT ${OUTPUT_FILES}
                     COMMAND ${COMMAND_TO_EXECUTE}
                     WORKING_DIRECTORY ${OUTPUT_DIR}
                     )

  # Generate the python script that converts the .hbk,.log pair into a .root file
  set(SCRIPT_TO_CONFIGURE "${RUNSMOP_PYTHON_DIR}/convert-hbk-log-to-root.py.in")
  get_filename_component(SCRIPT_NAME "${SCRIPT_TO_CONFIGURE}" NAME)
  string(REGEX REPLACE "^(.*)\\.[^.]+$" "\\1" SCRIPT_NAME ${SCRIPT_NAME})
  get_run_output_dir(OUTPUT_DIR ${RUN_NAME} ${VERSION})
  set(SCRIPT "${OUTPUT_DIR}/${RUN_NAME}-${SCRIPT_NAME}")
  configure_file("${SCRIPT_TO_CONFIGURE}" ${SCRIPT} @ONLY)

  add_custom_command(OUTPUT "${OUTPUT_ROOT_FILE}"
                     DEPENDS ${OUTPUT_FILES}
                     COMMAND ${PYTHON_EXECUTABLE} ${SCRIPT} "${OUTPUT_ROOT_FILE}" ${OUTPUT_FILES}
                     )

  if(SMOP_RUN_ALL)
  add_custom_target(${TARGET_NAME}
                    ALL
                    DEPENDS ${OUTPUT_DEPENDENCIES}
                    )
  else(SMOP_RUN_ALL)
  add_custom_target(${TARGET_NAME}
                    DEPENDS ${OUTPUT_DEPENDENCIES}
                    )
  endif(SMOP_RUN_ALL)

  set(BUILD_TARGET "build-SMoP_${_VERSION}")
  add_dependencies(${TARGET_NAME} ${BUILD_TARGET})

  if(SMOP_RUNS_${_VERSION})
    set(SMOP_RUNS_${_VERSION} "${SMOP_RUNS_${_VERSION}};${TARGET_NAME}" CACHE STRING "List of runs for SMoP version ${_VERSION}" FORCE)
  else(SMOP_RUNS_${_VERSION})
    set(SMOP_RUNS_${_VERSION} "${TARGET_NAME}" CACHE STRING "List of runs for SMoP version ${_VERSION}" FORCE)
  endif(SMOP_RUNS_${_VERSION})

  message(STATUS "... run ${RUN_NAME} will use ${SMOP_BINARY_FILE}")
  message(STATUS "... with the following input file: ${SMOP_INPUT_FILE}")
  message(STATUS "... and produce the following output files: ${OUTPUT_FILES}")
  message(STATUS "... which will be converted into the following ROOT output file: ${OUTPUT_ROOT_FILE}")
  message(STATUS "... depends on the following target: ${BUILD_TARGET}")

  if(SMOP_RUN_POSTPROCESS_RUN)
    postprocess_run(${SMOP_RUN_POSTPROCESS_RUN} ${_NAME} ${_VERSION} "${SMOP_PROJECTILE};${SMOP_TARGETA};${SMOP_TARGETZ};${SMOP_ENERGY};${SMOP_DEEXCITATION};${SMOP_NSHOTS}")
  endif(SMOP_RUN_POSTPROCESS_RUN)

endfunction(add_SMoP_run _NAME _VERSION SMOP_PROJECTILE SMOP_TARGETA SMOP_TARGETZ SMOP_ENERGY SMOP_DEEXCITATION SMOP_NSHOTS)



# Function that defines a SMoP run
#
# Syntax: SMoP_run(VERSIONS_FILE <filename> RUN_FILE <filename>)
function(SMoP_run)

  set(OPTIONS TIMED ALL)
  set(ONE_VALUE_ARGS VERSIONS_FILE RUN_FILE POSTPROCESS_RUN POSTPROCESS_BATCH LABEL)
  set(MULTI_VALUE_ARGS "")
  cmake_parse_arguments(SMOP_RUN "${OPTIONS}" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

  # LABEL is compulsory
  if(NOT SMOP_RUN_LABEL)
    message(FATAL_ERROR "Did not specity LABEL argument to SMoP_run")
    return()
  endif(NOT SMOP_RUN_LABEL)

  # Look for the time program
  if(SMOP_RUN_TIMED)
    find_program(TIME_EXECUTABLE time)
    if(NOT TIME_EXECUTABLE)
      message(WARNING "Cannot find time executable, timed runs will not be built")
      return()
    endif(NOT TIME_EXECUTABLE)
    set(TIME_OUTPUT_FORMAT "%U %S %e" CACHE STRING "Output format for the time command")
  endif(SMOP_RUN_TIMED)

  set(SMOP_THESE_VERSIONS "")
  file(STRINGS "${SMOP_RUN_VERSIONS_FILE}" VERSIONS_FILE)

  # Process the VERSIONS_FILE
  # Loop over the lines in the versions config file and construct targets
  foreach(LINE_UNSTRIPPED ${VERSIONS_FILE})

    string(STRIP ${LINE_UNSTRIPPED} LINE)

    string(SUBSTRING ${LINE} 0 1 FIRST_CHARACTER)

    if(NOT FIRST_CHARACTER STREQUAL "#")

      string(REGEX MATCHALL "([^\ ]+)" LINE_AS_LIST "${LINE}")
      list(LENGTH LINE_AS_LIST LIST_LENGTH)
      if(LIST_LENGTH LESS 2)
        message(FATAL_ERROR "Line\n  ${LINE}\nin file\n  ${SMOP_RUN_VERSIONS_FILE}\ncontains ${LIST_LENGTH} items but must contain 2 items:\n  VERSION_NAME\tGIT_VERSION")
      endif(LIST_LENGTH LESS 2)
      list(GET LINE_AS_LIST 0 VERSION_NAME)
      list(GET LINE_AS_LIST 1 GIT_VERSION)
      add_SMoP_version(${VERSION_NAME} ${GIT_VERSION})
      list(APPEND SMOP_THESE_VERSIONS ${VERSION_NAME})

    endif(NOT FIRST_CHARACTER STREQUAL "#")

  endforeach(LINE_UNSTRIPPED ${VERSIONS_FILE})

  # Process the RUN_FILE
  file(STRINGS "${SMOP_RUN_RUN_FILE}" RUN_FILE)

  # Loop over the lines in the versions config file and construct targets
  foreach(VERSION ${SMOP_THESE_VERSIONS})

    set(SMOP_RUNS_THIS_VERSION "")
    foreach(LINE ${RUN_FILE})
      string(REGEX MATCHALL "([^\ ]+)" LINE_AS_LIST "${LINE}")
      list(GET LINE_AS_LIST 0 RUN_NAME)
      list(GET LINE_AS_LIST 1 SMOP_PROJECTILE)
      list(GET LINE_AS_LIST 2 SMOP_TARGETA)
      list(GET LINE_AS_LIST 3 SMOP_TARGETZ)
      list(GET LINE_AS_LIST 4 SMOP_ENERGY)
      list(GET LINE_AS_LIST 5 SMOP_NSHOTS)
      list(GET LINE_AS_LIST 6 SMOP_DEEXCITATION)
      add_SMoP_run(${RUN_NAME} ${VERSION} ${SMOP_PROJECTILE} ${SMOP_TARGETA} ${SMOP_TARGETZ} ${SMOP_ENERGY} ${SMOP_DEEXCITATION} ${SMOP_NSHOTS})
      list(APPEND SMOP_RUNS_THIS_VERSION "${RUN_NAME}")
    endforeach(LINE ${RUN_FILE})

    if(SMOP_RUN_POSTPROCESS_BATCH)
      postprocess_batch("${SMOP_RUN_POSTPROCESS_BATCH}" "${SMOP_RUNS_THIS_VERSION}" "${VERSION}" "${SMOP_RUN_LABEL}")
    endif(SMOP_RUN_POSTPROCESS_BATCH)

  endforeach(VERSION ${SMOP_THESE_VERSIONS})

endfunction(SMoP_run)
