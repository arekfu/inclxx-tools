cmake_minimum_required(VERSION 2.8)

find_package(Git REQUIRED)

if(NOT INCLCASCADE_VERSION)
  message(FATAL_ERROR "BuildINCLCascade was called without defining the INCLCASCADE_VERSION cache variable")
endif(NOT INCLCASCADE_VERSION)

if(NOT INCLCASCADE_NAME)
  message(FATAL_ERROR "BuildINCLCascade was called without defining the INCLCASCADE_NAME cache variable")
endif(NOT INCLCASCADE_NAME)

if(NOT INCLCASCADE_GIT_REPOSITORY)
  set(INCLCASCADE_GIT_REPOSITORY "git@bitbucket.org:arekfu/incl.git")
endif(NOT INCLCASCADE_GIT_REPOSITORY)

if(NOT INCLCASCADE_BUILD_CONFIGURATION)
  set(INCLCASCADE_BUILD_CONFIGURATION "Release")
endif(NOT INCLCASCADE_BUILD_CONFIGURATION)


# Clone the git repository
set(CHECKOUT_DIR "${CMAKE_BINARY_DIR}/${INCLCASCADE_NAME}")
set(BUILD_DIR "${CMAKE_BINARY_DIR}/${INCLCASCADE_NAME}.build")
message(STATUS "Checkout directory: ${CHECKOUT_DIR}")
message(STATUS "Build directory: ${BUILD_DIR}")
execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory ${CHECKOUT_DIR})
execute_process(COMMAND ${GIT_EXECUTABLE} clone ${INCLCASCADE_GIT_REPOSITORY} "${CHECKOUT_DIR}"
                RESULT_VARIABLE CLONE_RESULT
                )
if(CLONE_RESULT)
  message(FATAL_ERROR "Cloning failed with error code ${CLONE_RESULT}")
endif(CLONE_RESULT)


# Check out the required version
execute_process(COMMAND ${GIT_EXECUTABLE} checkout "${INCLCASCADE_VERSION}"
                WORKING_DIRECTORY ${CHECKOUT_DIR}
                RESULT_VARIABLE CHECKOUT_RESULT
                )
if(CHECKOUT_RESULT)
  message(FATAL_ERROR "Checkout failed with error code ${CHECKOUT_RESULT}")
endif(CHECKOUT_RESULT)


# Initialise the required de-excitation submodules
if(INCLCASCADE_DEEXCITATION_MODULES)
  list(APPEND INCLCASCADE_BUILD_FLAGS "-DINCL_DEEXCITATION_USE:BOOL=ON")
endif(INCLCASCADE_DEEXCITATION_MODULES)

foreach(MODULE ${INCLCASCADE_DEEXCITATION_MODULES})
  string(TOUPPER ${MODULE} MODULE_UPPERCASE)
  string(TOLOWER ${MODULE} MODULE_LOWERCASE)

  list(APPEND INCLCASCADE_BUILD_FLAGS "-DINCL_DEEXCITATION_${MODULE_UPPERCASE}:BOOL=ON")

  if(NOT MODULE_UPPERCASE STREQUAL "NONE")
    execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init "de-excitation/${MODULE_LOWERCASE}/upstream"
                    WORKING_DIRECTORY ${CHECKOUT_DIR}
                    RESULT_VARIABLE SUBMODULE_UPDATE_RESULT
                    )
    if(SUBMODULE_UPDATE_RESULT)
      message(FATAL_ERROR "Initialisation for submodule ${MODULE} failed with error code ${SUBMODULE_UPDATE_RESULT}")
    endif(SUBMODULE_UPDATE_RESULT)
  endif(NOT MODULE_UPPERCASE STREQUAL "NONE")
endforeach(MODULE ${INCLCASCADE_DEEXCITATION_MODULES})



# Build INCLCascade
if(INCLCASCADE_ROOT_USE)
  list(APPEND INCLCASCADE_BUILD_FLAGS "-DINCL_ROOT_USE:BOOL=ON")
else(INCLCASCADE_ROOT_USE)
  list(APPEND INCLCASCADE_BUILD_FLAGS "-DINCL_ROOT_USE:BOOL=OFF")
endif(INCLCASCADE_ROOT_USE)
execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${BUILD_DIR})
execute_process(COMMAND ${CMAKE_COMMAND} "-DCMAKE_BUILD_TYPE:STRING=${INCLCASCADE_BUILD_CONFIGURATION}" ${INCLCASCADE_BUILD_FLAGS} "${CHECKOUT_DIR}"
                WORKING_DIRECTORY ${BUILD_DIR}
                RESULT_VARIABLE CONFIGURE_RESULT
                )
if(CONFIGURE_RESULT)
  message(FATAL_ERROR "Configure failed with error code ${CONFIGURE_RESULT}")
endif(CONFIGURE_RESULT)
execute_process(COMMAND ${CMAKE_COMMAND} --build "${BUILD_DIR}" --target "INCLCascade"
                WORKING_DIRECTORY ${BUILD_DIR}
                RESULT_VARIABLE BUILD_RESULT
                )
if(BUILD_RESULT)
  message(FATAL_ERROR "Build failed with error code ${BUILD_RESULT}")
endif(BUILD_RESULT)

