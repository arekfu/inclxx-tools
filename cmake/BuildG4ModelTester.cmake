cmake_minimum_required(VERSION 2.8)

find_package(Git REQUIRED)

if(NOT G4MODELTESTER_VERSION)
  set(G4MODELTESTER_VERSION "master")
endif(NOT G4MODELTESTER_VERSION)

if(NOT G4MODELTESTER_GIT_REPOSITORY)
  set(G4MODELTESTER_GIT_REPOSITORY "git@bitbucket.org:arekfu/g4modeltester.git")
endif(NOT G4MODELTESTER_GIT_REPOSITORY)

if(NOT G4MODELTESTER_BUILD_CONFIGURATION)
  set(G4MODELTESTER_BUILD_CONFIGURATION "Release")
endif(NOT G4MODELTESTER_BUILD_CONFIGURATION)


# Clone the git repository
set(G4MODELTESTER_NAME "G4ModelTester")
set(CHECKOUT_DIR "${CMAKE_BINARY_DIR}/${G4MODELTESTER_NAME}")
set(BUILD_DIR "${CHECKOUT_DIR}/${G4MODELTESTER_BUILD_CONFIGURATION}")
message(STATUS "Checkout directory: ${CHECKOUT_DIR}")
message(STATUS "Build directory: ${BUILD_DIR}")
execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory ${CHECKOUT_DIR})
execute_process(COMMAND ${GIT_EXECUTABLE} clone ${G4MODELTESTER_GIT_REPOSITORY} "${CHECKOUT_DIR}"
                RESULT_VARIABLE CLONE_RESULT
                )
if(CLONE_RESULT)
  message(FATAL_ERROR "Cloning failed with error code ${CLONE_RESULT}")
endif(CLONE_RESULT)


# Check out the required version
execute_process(COMMAND ${GIT_EXECUTABLE} checkout "${G4MODELTESTER_VERSION}"
                WORKING_DIRECTORY ${CHECKOUT_DIR}
                RESULT_VARIABLE CHECKOUT_RESULT
                )
if(CHECKOUT_RESULT)
  message(FATAL_ERROR "Checkout failed with error code ${CHECKOUT_RESULT}")
endif(CHECKOUT_RESULT)


# Build G4ModelTester
list(APPEND G4MODELTESTER_BUILD_FLAGS "-DGeant4_DIR:FILEPATH=${Geant4_DIR}")
execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${BUILD_DIR})
execute_process(COMMAND ${CMAKE_COMMAND} "-DCMAKE_BUILD_TYPE:STRING=${G4MODELTESTER_BUILD_CONFIGURATION}" ${G4MODELTESTER_BUILD_FLAGS} "${CHECKOUT_DIR}"
                WORKING_DIRECTORY ${BUILD_DIR}
                RESULT_VARIABLE CONFIGURE_RESULT
                )
if(CONFIGURE_RESULT)
  message(FATAL_ERROR "Configure failed with error code ${CONFIGURE_RESULT}")
endif(CONFIGURE_RESULT)
execute_process(COMMAND ${CMAKE_COMMAND} --build "${BUILD_DIR}" --target "G4ModelTester"
                WORKING_DIRECTORY ${BUILD_DIR}
                RESULT_VARIABLE BUILD_RESULT
                )
if(BUILD_RESULT)
  message(FATAL_ERROR "Build failed with error code ${BUILD_RESULT}")
endif(BUILD_RESULT)

