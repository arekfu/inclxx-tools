cmake_minimum_required(VERSION 2.8)

# Script for parsing function arguments
include(CMakeParseArguments)

include(RunINCLCascade)

# Get the directory of this module file
get_filename_component(RUNG4MODELTESTER_MODULE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
message(STATUS "RunG4ModelTester initialised from directory: ${RUNG4MODELTESTER_MODULE_DIR}")
get_filename_component(TOOLS_DIR "${RUNG4MODELTESTER_MODULE_DIR}" PATH)
set(RUNG4MODELTESTER_PYTHON_DIR "${TOOLS_DIR}/python")
message(STATUS "... will look for Python modules in directory: ${RUNG4MODELTESTER_PYTHON_DIR}")

set(G4MODELTESTER_BUILT OFF CACHE BOOL "Whether the build of G4ModelTester has already been requested" FORCE)
set(G4MODELTESTER_GENERATORS "" CACHE STRING "List of G4ModelTester generators" FORCE)

find_program(G4MODELTESTER_G4DATAENV "geant4-data-environment.sh")

####### Functions #######


# Function that adds build targets for a given G4ReactionXSec run
function(add_G4ReactionXSec_run _NAME _VERSION GENERATOR)

  # directly produces a processed run
  get_processed_run_target_name(TARGET_NAME ${_NAME} ${_VERSION})

  list(FIND G4REACTIONXSEC_RUNS_${_VERSION} ${TARGET_NAME} RUN_FOUND)
  if(NOT RUN_FOUND EQUAL "-1")
    message(STATUS "A G4ReactionXSec run called ${TARGET_NAME} already exists. Will not add a new one.")
    return()
  endif(NOT RUN_FOUND EQUAL "-1")

  message(STATUS "Adding ${TARGET_NAME} target...")

  get_script_run_output_file(OUTPUT_FILE ${_NAME} ${_VERSION})

  set(G4REACTIONXSEC_ARGS "${OUTPUT_FILE}" "${GENERATOR}" ${ARGN})

  set(COMMAND_TO_EXECUTE ${PYTHON_EXECUTABLE} ${G4REACTIONXSEC_WRAPPER_SCRIPT} ${G4REACTIONXSEC_ARGS}
                         )

  get_run_output_dir(OUTPUT_DIR ${_NAME} ${_VERSION})
  add_custom_command(OUTPUT ${OUTPUT_FILE}
                     COMMAND ${COMMAND_TO_EXECUTE}
                     WORKING_DIRECTORY ${OUTPUT_DIR}
                     )

  if(G4REACTIONXSEC_RUN_ALL)
  add_custom_target(${TARGET_NAME}
                    ALL
                    DEPENDS ${OUTPUT_FILE}
                    )
  else(G4REACTIONXSEC_RUN_ALL)
  add_custom_target(${TARGET_NAME}
                    DEPENDS ${OUTPUT_FILE}
                    )
  endif(G4REACTIONXSEC_RUN_ALL)

  if(G4REACTIONXSEC_RUNS_${_VERSION})
    set(G4REACTIONXSEC_RUNS_${_VERSION} "${G4REACTIONXSEC_RUNS_${_VERSION}};${TARGET_NAME}" CACHE STRING "List of runs for G4ReactionXSec version ${_VERSION}" FORCE)
  else(G4REACTIONXSEC_RUNS_${_VERSION})
    set(G4REACTIONXSEC_RUNS_${_VERSION} "${TARGET_NAME}" CACHE STRING "List of runs for G4ReactionXSec version ${_VERSION}" FORCE)
  endif(G4REACTIONXSEC_RUNS_${_VERSION})

  message(STATUS "... run ${TARGET_NAME} will use ${G4REACTIONXSEC_WRAPPER_SCRIPT}")
  message(STATUS "... with the following args: ${G4REACTIONXSEC_ARGS}")
  message(STATUS "... and produce the following output files: ${OUTPUT_FILE}")

endfunction(add_G4ReactionXSec_run _NAME _VERSION GENERATOR)



# Function that defines a G4ReactionXSec run
function(G4ReactionXSec_run)

  set(OPTIONS ALL)
  set(ONE_VALUE_ARGS LABEL GENERATORS_FILE RUN_FILE POSTPROCESS_BATCH)
  set(MULTI_VALUE_ARGS "")
  cmake_parse_arguments(G4REACTIONXSEC_RUN "${OPTIONS}" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

  # LABEL is compulsory
  if(NOT G4REACTIONXSEC_RUN_LABEL)
    message(FATAL_ERROR "Did not specity LABEL argument to G4ReactionXSec_run")
    return()
  endif(NOT G4REACTIONXSEC_RUN_LABEL)

  # GENERATORS_FILE is compulsory
  if(NOT G4REACTIONXSEC_RUN_GENERATORS_FILE)
    message(FATAL_ERROR "Did not specity GENERATORS_FILE argument to G4ReactionXSec_run")
    return()
  endif(NOT G4REACTIONXSEC_RUN_GENERATORS_FILE)

  # RUN_FILE is compulsory
  if(NOT G4REACTIONXSEC_RUN_RUN_FILE)
    message(FATAL_ERROR "Did not specity RUN_FILE argument to G4ReactionXSec_run")
    return()
  endif(NOT G4REACTIONXSEC_RUN_RUN_FILE)

  find_program(G4REACTIONXSEC_EXECUTABLE G4ReactionXSec)
  if(NOT G4REACTIONXSEC_EXECUTABLE)
    message(FATAL_ERROR "You must set the G4REACTIONXSEC_EXECUTABLE variable to the path to the G4ReactionXSec program")
  endif(NOT G4REACTIONXSEC_EXECUTABLE)

  # configure the INCLCascade-like wrapper
  set(FILE_TO_CONFIGURE "${RUNG4MODELTESTER_PYTHON_DIR}/G4ReactionXSec-INCLCascade-wrapper.py.in")
  get_filename_component(G4REACTIONXSEC_WRAPPER_SCRIPT_NAME "${FILE_TO_CONFIGURE}" NAME)
  string(REGEX REPLACE "^(.*)\\.[^.]+$" "\\1" G4REACTIONXSEC_WRAPPER_SCRIPT_NAME ${G4REACTIONXSEC_WRAPPER_SCRIPT_NAME})
  set(G4REACTIONXSEC_WRAPPER_SCRIPT "${CMAKE_CURRENT_BINARY_DIR}/${G4REACTIONXSEC_RUN_LABEL}-${G4REACTIONXSEC_WRAPPER_SCRIPT_NAME}")
  configure_file("${FILE_TO_CONFIGURE}" ${G4REACTIONXSEC_WRAPPER_SCRIPT} @ONLY)

  # Slurp the GENERATORS_FILE
  file(STRINGS "${G4REACTIONXSEC_RUN_GENERATORS_FILE}" GENERATORS_FILE)

  # Process the RUN_FILE
  file(STRINGS "${G4REACTIONXSEC_RUN_RUN_FILE}" RUN_FILE)

  # Loop over the lines in the generators file and construct targets
  foreach(GENERATOR ${GENERATORS_FILE})

    set(VERSION "G4RX-${GENERATOR}")

    set(G4REACTIONXSEC_RUNS_THIS_VERSION "")
    set(G4REACTIONXSEC_RUNS_${VERSION} "" CACHE STRING "List of runs for G4ReactionXSec version ${VERSION}" FORCE)
    foreach(LINE ${RUN_FILE})
      string(REGEX MATCHALL "([^\ ]+)" LINE_AS_LIST "${LINE}")
      list(GET LINE_AS_LIST 0 RUN_NAME)
      list(REMOVE_AT LINE_AS_LIST 0) # the rest of the list contains the CLI options
      add_G4ReactionXSec_run(${RUN_NAME} "${VERSION}" "${GENERATOR}" ${LINE_AS_LIST})
      list(APPEND G4REACTIONXSEC_RUNS_THIS_VERSION "${RUN_NAME}")
    endforeach(LINE ${RUN_FILE})

    if(G4REACTIONXSEC_RUN_POSTPROCESS_BATCH)
      postprocess_batch("${G4REACTIONXSEC_RUN_POSTPROCESS_BATCH}" "${G4REACTIONXSEC_RUNS_THIS_VERSION}" "${VERSION}" "${G4REACTIONXSEC_RUN_LABEL}")
    endif(G4REACTIONXSEC_RUN_POSTPROCESS_BATCH)

  endforeach(GENERATOR ${GENERATORS_FILE})

endfunction(G4ReactionXSec_run)


# Function that adds build targets for a given INCLCascade version
function(build_G4ModelTester)

  if(G4MODELTESTER_BUILT)
    message(STATUS "A build target for G4ModelTester has already been created. Will not add a new one.")
    return()
  endif(G4MODELTESTER_BUILT)

  set(G4MODELTESTER_BUILT ON CACHE BOOL "Whether the build of G4ModelTester has already been requested" FORCE)

  find_package(Geant4 REQUIRED)
  include(${Geant4_USE_FILE})

  message(STATUS "Adding build-G4ModelTester target...")

  set(G4MODELTESTER_BINARY_FILE "${PROJECT_BINARY_DIR}/G4ModelTester/Release/G4ModelTester" CACHE FILEPATH "Path to the G4ModelTester executable" FORCE)
  set(TARGET_NAME "build-G4ModelTester")

  add_custom_command(OUTPUT "${G4MODELTESTER_BINARY_FILE}"
                     COMMAND ${CMAKE_COMMAND}
                     -D "Geant4_DIR:FILEPATH=${Geant4_DIR}"
                     -P "${RUNG4MODELTESTER_MODULE_DIR}/BuildG4ModelTester.cmake"
                     WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
                     )

  add_custom_target(${TARGET_NAME}
                    DEPENDS "${G4MODELTESTER_BINARY_FILE}"
                    )

  message(STATUS "... build-G4ModelTester target")
  message(STATUS "... will build ${G4MODELTESTER_BINARY_FILE}")

endfunction(build_G4ModelTester)



# Function that adds generators
function(add_G4ModelTester_generator _NAME _GENERATOR)
  list(FIND G4MODELTESTER_GENERATORS ${_NAME} NAME_FOUND)
  if(NOT NAME_FOUND EQUAL "-1")
    message(STATUS "A G4ModelTester generator called ${_NAME} already exists. Will not add a new one.")
    return()
  endif(NOT NAME_FOUND EQUAL "-1")

  message(STATUS "Adding G4ModelTester generator ${_GENERATOR} called ${_NAME}...")

  set(G4MODELTESTER_RUNS_${_NAME} "" CACHE STRING "List of runs for G4ModelTester version ${_NAME}" FORCE)

  if(G4MODELTESTER_GENERATORS)
    set(G4MODELTESTER_GENERATORS "${G4MODELTESTER_GENERATORS};${_NAME}" CACHE STRING "List of G4ModelTester generators" FORCE)
  else(G4MODELTESTER_GENERATORS)
    set(G4MODELTESTER_GENERATORS "${_NAME}" CACHE STRING "List of G4ModelTester generators" FORCE)
  endif(G4MODELTESTER_GENERATORS)
endfunction(add_G4ModelTester_generator _NAME _GENERATOR)



# Function that adds build targets for a given G4ModelTester run
function(add_G4ModelTester_run _NAME _VERSION _GENERATOR)

  get_run_target_name(TARGET_NAME ${_NAME} ${_VERSION})

  list(FIND G4MODELTESTER_RUNS_${_VERSION} ${TARGET_NAME} RUN_FOUND)
  if(NOT RUN_FOUND EQUAL "-1")
    message(STATUS "A G4ModelTester run called ${TARGET_NAME} already exists. Will not add a new one.")
    return()
  endif(NOT RUN_FOUND EQUAL "-1")

  message(STATUS "Adding ${TARGET_NAME} target...")

  set(G4MODELTESTER_BINARY_FILE "${PROJECT_BINARY_DIR}/G4ModelTester/Release/G4ModelTester")

  set(G4MODELTESTER_ARGS -g "${_GENERATOR}" ${ARGN})

  get_G4MT_run_output_files(OUTPUT_FILES ${_NAME} ${_VERSION})

  if(G4MODELTESTER_RUN_TIMED)
    get_timed_run_output_file(OUTPUT_TIME_FILE ${RUN_NAME} ${VERSION})
    set(COMMAND_TO_EXECUTE ${TIME_EXECUTABLE} "-o${OUTPUT_TIME_FILE}" "-f${TIME_OUTPUT_FORMAT}"
                           ${PYTHON_EXECUTABLE} ${G4MODELTESTER_GENERATE_SCRIPT} ${G4MODELTESTER_ARGS}
                           )
  else(G4MODELTESTER_RUN_TIMED)
    set(COMMAND_TO_EXECUTE ${PYTHON_EXECUTABLE} ${G4MODELTESTER_GENERATE_SCRIPT} ${G4MODELTESTER_ARGS}
                           )
  endif(G4MODELTESTER_RUN_TIMED)

  get_run_output_dir(OUTPUT_DIR ${_NAME} ${_VERSION})
  add_custom_command(OUTPUT ${OUTPUT_FILES}
                     COMMAND ${COMMAND_TO_EXECUTE}
                     WORKING_DIRECTORY ${OUTPUT_DIR}
                     )

  if(G4MODELTESTER_RUN_ALL)
  add_custom_target(${TARGET_NAME}
                    ALL
                    DEPENDS ${OUTPUT_FILES} ${G4MODELTESTER_RUN_FILE}
                    )
  else(G4MODELTESTER_RUN_ALL)
  add_custom_target(${TARGET_NAME}
                    DEPENDS ${OUTPUT_FILES} ${G4MODELTESTER_RUN_FILE}
                    )
  endif(G4MODELTESTER_RUN_ALL)

  add_dependencies(${TARGET_NAME} "build-G4ModelTester")

  if(G4MODELTESTER_RUNS_${_VERSION})
    set(G4MODELTESTER_RUNS_${_VERSION} "${G4MODELTESTER_RUNS_${_VERSION}};${TARGET_NAME}" CACHE STRING "List of runs for G4ModelTester version ${_VERSION}" FORCE)
  else(G4MODELTESTER_RUNS_${_VERSION})
    set(G4MODELTESTER_RUNS_${_VERSION} "${TARGET_NAME}" CACHE STRING "List of runs for G4ModelTester version ${_VERSION}" FORCE)
  endif(G4MODELTESTER_RUNS_${_VERSION})

  message(STATUS "... run ${TARGET_NAME} will use ${G4MODELTESTER_GENERATE_SCRIPT}")
  message(STATUS "... with the following args: ${G4MODELTESTER_ARGS}")
  message(STATUS "... and produce the following output files: ${OUTPUT_FILES}")

  if(G4MODELTESTER_RUN_POSTPROCESS_RUN)
    postprocess_run(${G4MODELTESTER_RUN_POSTPROCESS_RUN} ${RUN_NAME} ${VERSION} ${LINE_AS_LIST})
  endif(G4MODELTESTER_RUN_POSTPROCESS_RUN)

endfunction(add_G4ModelTester_run _NAME _VERSION _GENERATOR)



# Function that defines a G4ModelTester run
function(G4ModelTester_run)

  set(OPTIONS TIMED ALL)
  set(ONE_VALUE_ARGS GENERATORS_FILE RUN_FILE POSTPROCESS_RUN POSTPROCESS_BATCH LABEL)
  set(MULTI_VALUE_ARGS "")
  cmake_parse_arguments(G4MODELTESTER_RUN "${OPTIONS}" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

  # LABEL is compulsory
  if(NOT G4MODELTESTER_RUN_LABEL)
    message(FATAL_ERROR "Did not specity LABEL argument to G4ModelTester_run")
    return()
  endif(NOT G4MODELTESTER_RUN_LABEL)

  # Look for the time program
  if(G4MODELTESTER_RUN_TIMED)
    find_program(TIME_EXECUTABLE time)
    if(NOT TIME_EXECUTABLE)
      message(WARNING "Cannot find time executable, timed runs will not be built")
      return()
    endif(NOT TIME_EXECUTABLE)
    set(TIME_OUTPUT_FORMAT "%U %S %e" CACHE STRING "Output format for the time command")
  endif(G4MODELTESTER_RUN_TIMED)

  build_G4ModelTester()

  # configure the INCLCascade-like wrapper
  set(FILE_TO_CONFIGURE "${RUNG4MODELTESTER_PYTHON_DIR}/G4ModelTester-INCLCascade-wrapper.py.in")
  get_filename_component(G4MODELTESTER_GENERATE_SCRIPT_NAME "${FILE_TO_CONFIGURE}" NAME)
  string(REGEX REPLACE "^(.*)\\.[^.]+$" "\\1" G4MODELTESTER_GENERATE_SCRIPT_NAME ${G4MODELTESTER_GENERATE_SCRIPT_NAME})
  set(G4MODELTESTER_GENERATE_SCRIPT "${CMAKE_CURRENT_BINARY_DIR}/${G4MODELTESTER_RUN_LABEL}-${G4MODELTESTER_GENERATE_SCRIPT_NAME}")
  configure_file("${FILE_TO_CONFIGURE}" ${G4MODELTESTER_GENERATE_SCRIPT} @ONLY)

  # Slurp the GENERATORS_FILE
  file(STRINGS "${G4MODELTESTER_RUN_GENERATORS_FILE}" GENERATORS_FILE)

  # Process the RUN_FILE
  file(STRINGS "${G4MODELTESTER_RUN_RUN_FILE}" RUN_FILE)

  # Loop over the lines in the generators file and construct targets
  foreach(GENERATOR_LINE ${GENERATORS_FILE})

    string(REGEX MATCHALL "([^\ ]+)" GENERATOR_LINE_AS_LIST "${GENERATOR_LINE}")
    list(GET GENERATOR_LINE_AS_LIST 0 VERSION)
    list(GET GENERATOR_LINE_AS_LIST 1 GENERATOR)
    set(EXTRA_ARGS ${GENERATOR_LINE_AS_LIST})
    list(REMOVE_AT EXTRA_ARGS 0)
    list(REMOVE_AT EXTRA_ARGS 0)

    add_G4ModelTester_generator(${VERSION} ${GENERATOR})

    set(G4MODELTESTER_RUNS_THIS_VERSION "")
    foreach(LINE ${RUN_FILE})
      string(REGEX MATCHALL "([^\ ]+)" LINE_AS_LIST "${LINE}")
      list(GET LINE_AS_LIST 0 RUN_NAME)
      list(REMOVE_AT LINE_AS_LIST 0) # the rest of the list contains the CLI options
      list(APPEND LINE_AS_LIST ${EXTRA_ARGS}) # append the extra args contained in the generators file
      add_G4ModelTester_run(${RUN_NAME} ${VERSION} ${GENERATOR} ${LINE_AS_LIST})
      list(APPEND G4MODELTESTER_RUNS_THIS_VERSION "${RUN_NAME}")
    endforeach(LINE ${RUN_FILE})

    if(G4MODELTESTER_RUN_POSTPROCESS_BATCH)
      postprocess_batch("${G4MODELTESTER_RUN_POSTPROCESS_BATCH}" "${G4MODELTESTER_RUNS_THIS_VERSION}" "${VERSION}" "${G4MODELTESTER_RUN_LABEL}")
    endif(G4MODELTESTER_RUN_POSTPROCESS_BATCH)

  endforeach(GENERATOR_LINE ${GENERATORS_FILE})

endfunction(G4ModelTester_run)

