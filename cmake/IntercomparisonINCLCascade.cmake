cmake_minimum_required(VERSION 2.8)

# Script for parsing function arguments
include(CMakeParseArguments)

# Functions to generate INCL++ runs
include(RunSMoP)
include(RunINCLCascade)
include(RunG4ModelTester)

# Needs Python
find_package(PythonInterp REQUIRED)

# Get the directory of this module file
get_filename_component(INTERCOMPARISON_MODULE_DIR ${CMAKE_CURRENT_LIST_FILE} PATH)
message(STATUS "IntercomparisonINCLCascade initialised from directory: ${INTERCOMPARISON_MODULE_DIR}")
get_filename_component(TOOLS_DIR ${INTERCOMPARISON_MODULE_DIR} PATH)
set(INTERCOMPARISON_PYTHON_DIR "${TOOLS_DIR}/python")
message(STATUS "... will look for Python modules in directory: ${INTERCOMPARISON_PYTHON_DIR}")

# Needs pdftk for handling pdf files
find_program(PDFTK_EXECUTABLE pdftk)



####### Functions #######


# Function that defines the path to the pickled output file
function(get_pickled_output_file VAR INTERCOMPARISON_LABEL VERSION_NAME)
  set(${VAR} "${CMAKE_BINARY_DIR}/pickled-${INTERCOMPARISON_LABEL}-${VERSION_NAME}.dat" PARENT_SCOPE)
endfunction(get_pickled_output_file VAR INTERCOMPARISON_LABEL VERSION_NAME)



# Function that adds build targets for a given INCLCascade version
function(intercomparison)

  set(OPTIONS "")
  set(ONE_VALUE_ARGS INCLCASCADE_VERSIONS_FILE G4MODELTESTER_GENERATORS_FILE SMOP_VERSIONS_FILE DDXS_LINX DDXS_LOGX ISOT DDXSPT LABEL)
  set(MULTI_VALUE_ARGS "")
  cmake_parse_arguments(INTERCOMPARISON "${OPTIONS}" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

  # LABEL is compulsory
  if(NOT INTERCOMPARISON_LABEL)
    message(FATAL_ERROR "Did not specity LABEL argument to intercomparison")
    return()
  endif(NOT INTERCOMPARISON_LABEL)

  # RUNNER defaults to inclxx
  if((INTERCOMPARISON_INCLCASCADE_VERSIONS_FILE AND INTERCOMPARISON_G4MODELTESTER_GENERATORS_FILE)
    OR (INTERCOMPARISON_INCLCASCADE_VERSIONS_FILE AND INTERCOMPARISON_SMOP_VERSIONS_FILE)
    OR (INTERCOMPARISON_SMOP_VERSIONS_FILE AND INTERCOMPARISON_G4MODELTESTER_GENERATORS_FILE))
    message(FATAL_ERROR "Must specify only one of INCLCASCADE_VERSIONS_FILE, SMOP_VERSIONS_FILE and G4MODELTESTER_GENERATORS_FILE")
    return()
  endif()

  set(SCRIPT "${INTERCOMPARISON_PYTHON_DIR}/pickle-list_syst_data.py")

  if(INTERCOMPARISON_INCLCASCADE_VERSIONS_FILE)
    file(STRINGS "${INTERCOMPARISON_INCLCASCADE_VERSIONS_FILE}" VERSIONS_FILE)
  elseif(INTERCOMPARISON_SMOP_VERSIONS_FILE)
    file(STRINGS "${INTERCOMPARISON_SMOP_VERSIONS_FILE}" VERSIONS_FILE)
  elseif(INTERCOMPARISON_G4MODELTESTER_GENERATORS_FILE)
    file(STRINGS "${INTERCOMPARISON_G4MODELTESTER_GENERATORS_FILE}" VERSIONS_FILE)
  endif(INTERCOMPARISON_INCLCASCADE_VERSIONS_FILE)

  # Process the VERSIONS_FILE
  # Loop over the lines in the versions config file and construct run.conf and pickled files
  foreach(LINE_UNSTRIPPED ${VERSIONS_FILE})

    string(STRIP ${LINE_UNSTRIPPED} LINE)

    string(SUBSTRING ${LINE} 0 1 FIRST_CHARACTER)

    if(NOT FIRST_CHARACTER STREQUAL "#")

      string(REGEX MATCHALL "([^\ ]+)" LINE_AS_LIST "${LINE}")
      list(GET LINE_AS_LIST 0 VERSION_NAME)

      set(RUN_OUTPUT_FILE "${CMAKE_BINARY_DIR}/run-${INTERCOMPARISON_LABEL}-${VERSION_NAME}.conf")
      get_pickled_output_file(PICKLED_OUTPUT_FILE ${INTERCOMPARISON_LABEL} ${VERSION_NAME})
      set(OUTPUT_FILES ${RUN_OUTPUT_FILE} ${PICKLED_OUTPUT_FILE})

      set(COMMAND_LINE ${PYTHON_EXECUTABLE} ${SCRIPT} "${VERSION_NAME}" ${INTERCOMPARISON_LABEL})
      if(INTERCOMPARISON_DDXS_LINX)
        list(APPEND COMMAND_LINE --ddxslinx "${INTERCOMPARISON_DDXS_LINX}")
      endif(INTERCOMPARISON_DDXS_LINX)
      if(INTERCOMPARISON_DDXS_LOGX)
        list(APPEND COMMAND_LINE --ddxslogx "${INTERCOMPARISON_DDXS_LOGX}")
      endif(INTERCOMPARISON_DDXS_LOGX)
      if(INTERCOMPARISON_ISOT)
        list(APPEND COMMAND_LINE --isot "${INTERCOMPARISON_ISOT}")
      endif(INTERCOMPARISON_ISOT)
      if(INTERCOMPARISON_DDXSPT)
        list(APPEND COMMAND_LINE --ddxspt "${INTERCOMPARISON_DDXSPT}")
      endif(INTERCOMPARISON_DDXSPT)
      message(STATUS "Producing pickled output file: ${PICKLED_OUTPUT_FILE}")
      message(STATUS "  ... with the following command line: ${COMMAND_LINE}")
      execute_process(COMMAND ${COMMAND_LINE}
                      WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                      )

    endif(NOT FIRST_CHARACTER STREQUAL "#")

  endforeach(LINE_UNSTRIPPED ${VERSIONS_FILE})

  if(INTERCOMPARISON_INCLCASCADE_VERSIONS_FILE)
    INCLCascade_run(VERSIONS_FILE ${INTERCOMPARISON_INCLCASCADE_VERSIONS_FILE}
                    RUN_FILE ${RUN_OUTPUT_FILE}
                    LABEL ${INTERCOMPARISON_LABEL}
                    POSTPROCESS_RUN "${INTERCOMPARISON_PYTHON_DIR}/produce-ROOT-histos.py.in"
                    POSTPROCESS_BATCH "${INTERCOMPARISON_PYTHON_DIR}/merge-ROOT-files.py.in"
                    )
  elseif(INTERCOMPARISON_G4MODELTESTER_GENERATORS_FILE)
    G4ModelTester_run(GENERATORS_FILE ${INTERCOMPARISON_G4MODELTESTER_GENERATORS_FILE}
                      RUN_FILE ${RUN_OUTPUT_FILE}
                      LABEL ${INTERCOMPARISON_LABEL}
                      POSTPROCESS_RUN "${INTERCOMPARISON_PYTHON_DIR}/produce-ROOT-histos.py.in"
                      POSTPROCESS_BATCH "${INTERCOMPARISON_PYTHON_DIR}/merge-ROOT-files.py.in"
                      )
  elseif(INTERCOMPARISON_SMOP_VERSIONS_FILE)
    SMoP_run(VERSIONS_FILE ${INTERCOMPARISON_SMOP_VERSIONS_FILE}
                    RUN_FILE ${RUN_OUTPUT_FILE}
                    LABEL ${INTERCOMPARISON_LABEL}
                    POSTPROCESS_RUN "${INTERCOMPARISON_PYTHON_DIR}/produce-ROOT-histos.py.in"
                    POSTPROCESS_BATCH "${INTERCOMPARISON_PYTHON_DIR}/merge-ROOT-files.py.in"
                    )
  endif(INTERCOMPARISON_INCLCASCADE_VERSIONS_FILE)

endfunction(intercomparison)



function(intercomparison_plot)
  if(NOT PDFTK_EXECUTABLE)
    message(WARNING "pdftk executable not found, will not be able to concatenate pdf plots")
  endif(NOT PDFTK_EXECUTABLE)

  set(OPTIONS "")
  set(ONE_VALUE_ARGS INTERCOMPARISON_LABEL)
  set(MULTI_VALUE_ARGS VERSIONS LABELS)
  cmake_parse_arguments(INTERCOMPARISON_PLOT "${OPTIONS}" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

  list(LENGTH INTERCOMPARISON_PLOT_VERSIONS NUMBER_VERSIONS)
  if(NOT NUMBER_VERSIONS EQUAL 2)
    message(FATAL_ERROR "Must call intercomparison_plot with exactly two VERSIONS arguments")
    return()
  endif(NOT NUMBER_VERSIONS EQUAL 2)

  list(LENGTH INTERCOMPARISON_PLOT_LABELS NUMBER_LABELS)
  if(NOT NUMBER_LABELS EQUAL 2)
    message(FATAL_ERROR "Must call intercomparison_plot with exactly two LABELS arguments")
    return()
  endif(NOT NUMBER_LABELS EQUAL 2)

  if(NOT INTERCOMPARISON_PLOT_INTERCOMPARISON_LABEL)
    message(FATAL_ERROR "Must provide the INTERCOMPARISON_LABEL argument")
    return()
  endif(NOT INTERCOMPARISON_PLOT_INTERCOMPARISON_LABEL)

  list(GET INTERCOMPARISON_PLOT_VERSIONS 0 VERSION1)
  list(GET INTERCOMPARISON_PLOT_VERSIONS 1 VERSION2)
  list(GET INTERCOMPARISON_PLOT_LABELS 0 LABEL1)
  list(GET INTERCOMPARISON_PLOT_LABELS 1 LABEL2)

  set(VERSION_LABEL1 "${VERSION1}-${INTERCOMPARISON_PLOT_INTERCOMPARISON_LABEL}")
  set(VERSION_LABEL2 "${VERSION2}-${INTERCOMPARISON_PLOT_INTERCOMPARISON_LABEL}")

  get_script_batch_output_file(BATCH_OUTPUT_FILE1 "${VERSION_LABEL1}")
  get_script_batch_output_file(BATCH_OUTPUT_FILE2 "${VERSION_LABEL2}")

  get_processed_batch_target_name(BATCH_TARGET_NAME1 "${VERSION_LABEL1}")
  get_processed_batch_target_name(BATCH_TARGET_NAME2 "${VERSION_LABEL2}")

  get_plot_output_dir(PLOT_OUTPUT_DIR "${VERSION_LABEL1}" "${VERSION_LABEL2}")
  get_plot_output_file(PLOT_OUTPUT_FILE "${VERSION_LABEL1}" "${VERSION_LABEL2}")
  get_plot_target_name(PLOT_TARGET_NAME "${VERSION_LABEL1}" "${VERSION_LABEL2}")

  set(FILE_TO_CONFIGURE "${INTERCOMPARISON_PYTHON_DIR}/compare-ROOT-runs.py.in")
  get_filename_component(INTERCOMPARISON_PLOT_SCRIPT_NAME "${FILE_TO_CONFIGURE}" NAME)
  string(REGEX REPLACE "^(.*)\\.[^.]+$" "\\1" INTERCOMPARISON_PLOT_SCRIPT_NAME ${INTERCOMPARISON_PLOT_SCRIPT_NAME})
  set(INTERCOMPARISON_PLOT_SCRIPT "${PLOT_OUTPUT_DIR}/${INTERCOMPARISON_PLOT_SCRIPT_NAME}")
  configure_file("${FILE_TO_CONFIGURE}" "${INTERCOMPARISON_PLOT_SCRIPT}" @ONLY)

  set(COMMAND_LINE ${PYTHON_EXECUTABLE} ${INTERCOMPARISON_PLOT_SCRIPT} "${BATCH_OUTPUT_FILE1}" "${BATCH_OUTPUT_FILE2}" "${LABEL1}" "${LABEL2}" "${PLOT_OUTPUT_FILE}")
  add_custom_command(OUTPUT "${PLOT_OUTPUT_FILE}"
                     COMMAND ${COMMAND_LINE}
                     )

  add_custom_target(${PLOT_TARGET_NAME}
                    ALL
                    DEPENDS "${PLOT_OUTPUT_FILE}"
                    )
  message(STATUS "Added intercomparison_plot target: ${PLOT_TARGET_NAME}")
  message(STATUS " ... will produce the following output file: ${PLOT_OUTPUT_FILE}")
  message(STATUS " ... depends on the following targets: ${BATCH_TARGET_NAME1} ${BATCH_TARGET_NAME2}")

  add_dependencies(${PLOT_TARGET_NAME} ${BATCH_TARGET_NAME1} ${BATCH_TARGET_NAME2})

endfunction(intercomparison_plot)

