cmake_minimum_required(VERSION 2.8)

if(NOT SMOP_VERSION)
  message(FATAL_ERROR "BuildSMoP was called without defining the SMOP_VERSION cache variable")
endif(NOT SMOP_VERSION)

if(NOT SMOP_NAME)
  message(FATAL_ERROR "BuildSMoP was called without defining the SMOP_NAME cache variable")
endif(NOT SMOP_NAME)

find_package(Git REQUIRED)

find_program(MAKE_EXECUTABLE make)
if(NOT MAKE_EXECUTABLE)
  message(FATAL_ERROR "Cannot find make program to build SMoP")
endif(NOT MAKE_EXECUTABLE)

if(NOT SMOP_GIT_REPOSITORY)
  set(SMOP_GIT_REPOSITORY "git@bitbucket.org:arekfu/smop.git")
endif(NOT SMOP_GIT_REPOSITORY)


# Clone the git repository
set(CHECKOUT_DIR "${CMAKE_BINARY_DIR}/${SMOP_NAME}")
set(BUILD_DIR "${CHECKOUT_DIR}/src")
message(STATUS "Checkout directory: ${CHECKOUT_DIR}")
message(STATUS "Build directory: ${BUILD_DIR}")
execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory ${CHECKOUT_DIR})
execute_process(COMMAND ${GIT_EXECUTABLE} clone "${SMOP_GIT_REPOSITORY}" "${CHECKOUT_DIR}"
                RESULT_VARIABLE CLONE_RESULT
                )
if(CLONE_RESULT)
  message(FATAL_ERROR "Cloning failed with error code ${CLONE_RESULT}")
endif(CLONE_RESULT)



# Check out the required version
configure_file("${TOOLS_DIR}/smop-post-checkout.sh" "${CHECKOUT_DIR}/.git/hooks/post-checkout" COPYONLY)
execute_process(COMMAND ${GIT_EXECUTABLE} checkout "${SMOP_VERSION}"
                WORKING_DIRECTORY ${CHECKOUT_DIR}
                RESULT_VARIABLE CHECKOUT_RESULT
                )
if(CHECKOUT_RESULT)
  message(FATAL_ERROR "Checkout failed with error code ${CHECKOUT_RESULT}")
endif(CHECKOUT_RESULT)



# Build SMoP
execute_process(COMMAND pwd
                WORKING_DIRECTORY ${BUILD_DIR}
                )
execute_process(COMMAND ${MAKE_EXECUTABLE} "PWD=${BUILD_DIR}" # horrible hack to override the PWD variable in the SMoP Makefile
                WORKING_DIRECTORY ${BUILD_DIR}
                RESULT_VARIABLE BUILD_RESULT
                )
if(BUILD_RESULT)
  message(FATAL_ERROR "Build failed with error code ${BUILD_RESULT}")
endif(BUILD_RESULT)

file(GLOB SMOP_EXECUTABLE ${CHECKOUT_DIR}/smop_*_*)
execute_process(COMMAND ${CMAKE_COMMAND} -E rename ${SMOP_EXECUTABLE} ${CHECKOUT_DIR}/smop)
