cmake_minimum_required(VERSION 2.8)

# Script for parsing function arguments
include(CMakeParseArguments)

# Functions to generate run files from Python scripts, to generate target and directory names...
include(CommonINCLCascadeSMoP)

# Get the directory of this module file
get_filename_component(RUNINCLCASCADE_MODULE_DIR ${CMAKE_CURRENT_LIST_FILE} PATH)
message(STATUS "RunINCLCascade initialised from directory: ${RUNINCLCASCADE_MODULE_DIR}")
get_filename_component(TOOLS_DIR ${RUNINCLCASCADE_MODULE_DIR} PATH)
set(RUNINCLCASCADE_PYTHON_DIR "${TOOLS_DIR}/python")
message(STATUS "... will look for Python modules in directory: ${RUNINCLCASCADE_PYTHON_DIR}")

set(INCLCASCADE_VERSIONS "" CACHE STRING "List of INCLCascade versions" FORCE)


####### Functions #######

function(get_INCLCascade_deexcitation_option VAR MODEL)
  if(MODEL MATCHES ablaxx)
    set(${VAR} ablav3p PARENT_SCOPE)
  else(MODEL MATCHES ablaxx)
    set(${VAR} ${INCLCASCADE_DEEXCITATION_${_VERSION}} PARENT_SCOPE)
  endif(MODEL MATCHES ablaxx)
endfunction(get_INCLCascade_deexcitation_option VAR MODEL)

function(get_INCLCascade_source_dir _DIR _NAME)
  set(${_DIR} "${PROJECT_BINARY_DIR}/${_NAME}" PARENT_SCOPE)
endfunction(get_INCLCascade_source_dir _DIR _NAME)

function(get_INCLCascade_binary_dir _DIR _NAME)
  set(${_DIR} "${PROJECT_BINARY_DIR}/${_NAME}.build" PARENT_SCOPE)
endfunction(get_INCLCascade_binary_dir _DIR _NAME)

function(get_INCLCascade_binary _BINARY _NAME)
  get_INCLCascade_binary_dir(DIR ${_NAME})
  set(${_BINARY} "${DIR}/INCLCascade" PARENT_SCOPE)
endfunction(get_INCLCascade_binary _BINARY _NAME)

# Generate an inclxxrc file for the given INCL++ version
function(generate_inclxxrc_file INCLXXRC_FILE VERSION_NAME)
  get_INCLCascade_source_dir(SOURCE_DIR ${VERSION_NAME})
  get_INCLCascade_binary_dir(BINARY_DIR ${VERSION_NAME})
  set(OUTPUT_FILE "${BINARY_DIR}/inclxxrc")
  set(${INCLXXRC_FILE} "${OUTPUT_FILE}" PARENT_SCOPE)
  set(INCLXXRC_CONTENT "inclxx-datafile-path=${SOURCE_DIR}/data\n")
  set(INCLXXRC_CONTENT "${INCLXXRC_CONTENT}abla07-datafile-path=${SOURCE_DIR}/de-excitation/abla07/upstream/tables/\n")
  set(INCLXXRC_CONTENT "${INCLXXRC_CONTENT}ablav3p-cxx-datafile-path=${SOURCE_DIR}/de-excitation/ablaxx/upstream/data/G4ABLA3.0/\n")
  set(INCLXXRC_CONTENT "${INCLXXRC_CONTENT}geminixx-datafile-path=${SOURCE_DIR}/de-excitation/geminixx/upstream/\n")
  file(WRITE ${OUTPUT_FILE} ${INCLXXRC_CONTENT})
endfunction(generate_inclxxrc_file INCLXXRC_FILE VERSION_NAME)

# Function that adds build targets for a given INCLCascade version
function(add_INCLCascade_version _NAME _VERSION _DEEXCITATION _INCLXXRC)

  list(FIND INCLCASCADE_VERSIONS ${_NAME} VERSION_FOUND)
  if(NOT VERSION_FOUND EQUAL "-1")
    message(STATUS "An INCLCascade version called ${_NAME} already exists. Will not add a new one.")
    return()
  endif(NOT VERSION_FOUND EQUAL "-1")

  message(STATUS "Adding build-INCLCascade_${_NAME} target...")

  set(INCLCASCADE_RUNS_${VERSION_NAME} "" CACHE STRING "List of runs for INCLCascade version ${VERSION_NAME}" FORCE)

  get_INCLCascade_binary(INCLCASCADE_BINARY_FILE ${_NAME})
  set(TARGET_NAME "build-INCLCascade_${_NAME}")

  add_custom_command(OUTPUT "${INCLCASCADE_BINARY_FILE}"
                     COMMAND ${CMAKE_COMMAND}
                     -D "INCLCASCADE_VERSION:STRING=${_VERSION}"
                     -D "INCLCASCADE_NAME:STRING=${_NAME}"
                     -D "INCLCASCADE_BUILD_FLAGS:STRING=\"${INCLCASCADE_BUILD_FLAGS}\""
                     -D "INCLCASCADE_DEEXCITATION_MODULES:STRING=\"${_DEEXCITATION}\""
                     -D "INCLCASCADE_GIT_REPOSITORY:STRING=${INCLCASCADE_GIT_REPOSITORY}"
                     -D "INCLCASCADE_ROOT_USE:BOOL=${INCLCASCADE_ROOT_USE}"
                     -P "${RUNINCLCASCADE_MODULE_DIR}/BuildINCLCascade.cmake"
                     WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
                     )

  add_custom_target(${TARGET_NAME}
                    DEPENDS "${INCLCASCADE_BINARY_FILE}"
                    )

  if(INCLCASCADE_VERSIONS)
    set(INCLCASCADE_VERSIONS "${INCLCASCADE_VERSIONS};${_NAME}" CACHE STRING "List of INCLCascade versions" FORCE)
  else(INCLCASCADE_VERSIONS)
    set(INCLCASCADE_VERSIONS "${_NAME}" CACHE STRING "List of INCLCascade versions" FORCE)
  endif(INCLCASCADE_VERSIONS)

  set(INCLCASCADE_GIT_VERSION_${_NAME} ${_VERSION} CACHE STRING "Git version of version ${_NAME}" FORCE)
  string(TOLOWER ${_DEEXCITATION} LOWER_DEEXCITATION)
  set(INCLCASCADE_DEEXCITATION_${_NAME} ${LOWER_DEEXCITATION} CACHE STRING "De-excitation model for version ${_NAME}" FORCE)
  set(INCLCASCADE_INCLXXRC_FILE_${_NAME} ${_INCLXXRC} CACHE STRING "Path to the inclxxrc file for version ${_NAME}" FORCE)
  set(INCLCASCADE_EXTRA_ARGS_${_NAME} ${ARGN} CACHE STRING "Extra arguments for version ${_NAME}" FORCE)
  message(STATUS "... version ${_NAME}: build-INCLCascade_${_VERSION} target, \"${_DEEXCITATION}\" deexcitation model, ${_INCLXXRC} config file")
  message(STATUS "... will build ${INCLCASCADE_BINARY_FILE}")

endfunction(add_INCLCascade_version _NAME _VERSION _DEEXCITATION _INCLXXRC)



# Function that adds build targets for a given INCLCascade run
function(add_INCLCascade_run _NAME _VERSION)

  get_run_target_name(TARGET_NAME ${_NAME} ${_VERSION})

  list(FIND INCLCASCADE_RUNS_${_VERSION} ${TARGET_NAME} RUN_FOUND)
  if(NOT RUN_FOUND EQUAL "-1")
    message(STATUS "An INCLCascade run called ${TARGET_NAME} already exists. Will not add a new one.")
    return()
  endif(NOT RUN_FOUND EQUAL "-1")

  message(STATUS "Adding ${TARGET_NAME} target...")

  get_INCLCascade_binary(INCLCASCADE_BINARY_FILE ${_VERSION})

  get_run_output_files(OUTPUT_FILES ${_NAME} ${_VERSION} "INCLCascade")

  get_INCLCascade_deexcitation_option(INCLCASCADE_DEEXCITATION_ARG ${INCLCASCADE_DEEXCITATION_${_VERSION}})
  set(INCLCASCADE_ARGS ${ARGN} "--de-excitation=${INCLCASCADE_DEEXCITATION_ARG}" ${INCLCASCADE_EXTRA_ARGS_${_VERSION}})

  if(INCLCASCADE_RUN_TIMED)
    get_timed_run_output_file(OUTPUT_TIME_FILE ${_NAME} ${_VERSION})
    set(COMMAND_TO_EXECUTE "INCLXXRC=${INCLCASCADE_INCLXXRC_FILE_${_VERSION}}"
                           ${TIME_EXECUTABLE} "-o${OUTPUT_TIME_FILE}" "-f${TIME_OUTPUT_FORMAT}"
                           ${INCLCASCADE_BINARY_FILE} ${INCLCASCADE_ARGS}
                           )
  else(INCLCASCADE_RUN_TIMED)
    set(COMMAND_TO_EXECUTE "INCLXXRC=${INCLCASCADE_INCLXXRC_FILE_${_VERSION}}"
                           ${INCLCASCADE_BINARY_FILE} ${INCLCASCADE_ARGS}
                           )
  endif(INCLCASCADE_RUN_TIMED)

  get_run_output_dir(OUTPUT_DIR ${_NAME} ${_VERSION})
  add_custom_command(OUTPUT ${OUTPUT_FILES}
                     COMMAND ${COMMAND_TO_EXECUTE}
                     WORKING_DIRECTORY ${OUTPUT_DIR}
                     )

  if(INCLCASCADE_RUN_ALL)
  add_custom_target(${TARGET_NAME}
                    ALL
                    DEPENDS ${OUTPUT_FILES} ${INCLCASCADE_RUN_FILE}
                    )
  else(INCLCASCADE_RUN_ALL)
  add_custom_target(${TARGET_NAME}
                    DEPENDS ${OUTPUT_FILES} ${INCLCASCADE_RUN_FILE}
                    )
  endif(INCLCASCADE_RUN_ALL)

  set(BUILD_TARGET "build-INCLCascade_${_VERSION}")
  add_dependencies(${TARGET_NAME} ${BUILD_TARGET})

  if(INCLCASCADE_RUNS_${_VERSION})
    set(INCLCASCADE_RUNS_${_VERSION} "${INCLCASCADE_RUNS_${_VERSION}};${TARGET_NAME}" CACHE STRING "List of runs for INCLCascade version ${_VERSION}" FORCE)
  else(INCLCASCADE_RUNS_${_VERSION})
    set(INCLCASCADE_RUNS_${_VERSION} "${TARGET_NAME}" CACHE STRING "List of runs for INCLCascade version ${_VERSION}" FORCE)
  endif(INCLCASCADE_RUNS_${_VERSION})

  message(STATUS "... run ${TARGET_NAME} will use ${INCLCASCADE_BINARY_FILE}")
  message(STATUS "... with the following args: ${INCLCASCADE_ARGS}")
  message(STATUS "... and produce the following output files: ${OUTPUT_FILES}")
  message(STATUS "... depends on the following target: ${BUILD_TARGET}")

  if(INCLCASCADE_RUN_POSTPROCESS_RUN)
    postprocess_run(${INCLCASCADE_RUN_POSTPROCESS_RUN} ${_NAME} ${_VERSION} ${ARGN})
  endif(INCLCASCADE_RUN_POSTPROCESS_RUN)

endfunction(add_INCLCascade_run _NAME _VERSION)



# Function that defines an INCLCascade run
#
# Syntax: INCLCascade_run(VERSIONS_FILE <filename> RUN_FILE <filename>)
function(INCLCascade_run)

  set(OPTIONS TIMED ALL)
  set(ONE_VALUE_ARGS VERSIONS_FILE RUN_FILE POSTPROCESS_RUN POSTPROCESS_BATCH LABEL)
  set(MULTI_VALUE_ARGS "")
  cmake_parse_arguments(INCLCASCADE_RUN "${OPTIONS}" "${ONE_VALUE_ARGS}" "${MULTI_VALUE_ARGS}" ${ARGN})

  # LABEL is compulsory
  if(NOT INCLCASCADE_RUN_LABEL)
    message(FATAL_ERROR "Did not specity LABEL argument to INCLCascade_run")
    return()
  endif(NOT INCLCASCADE_RUN_LABEL)

  # Look for the time program
  if(INCLCASCADE_RUN_TIMED)
    find_program(TIME_EXECUTABLE time)
    if(NOT TIME_EXECUTABLE)
      message(WARNING "Cannot find time executable, timed runs will not be built")
      return()
    endif(NOT TIME_EXECUTABLE)
    set(TIME_OUTPUT_FORMAT "%U %S %e" CACHE STRING "Output format for the time command")
  endif(INCLCASCADE_RUN_TIMED)

  set(INCLCASCADE_THESE_VERSIONS "")
  file(STRINGS "${INCLCASCADE_RUN_VERSIONS_FILE}" VERSIONS_FILE)

  # Process the VERSIONS_FILE
  # Loop over the lines in the versions config file and construct targets
  foreach(LINE_UNSTRIPPED ${VERSIONS_FILE})

    string(STRIP ${LINE_UNSTRIPPED} LINE)

    string(SUBSTRING ${LINE} 0 1 FIRST_CHARACTER)

    if(NOT FIRST_CHARACTER STREQUAL "#")

      string(REGEX MATCHALL "([^\ ]+)" LINE_AS_LIST "${LINE}")
      list(LENGTH LINE_AS_LIST LIST_LENGTH)
      if(LIST_LENGTH LESS 3)
        message(FATAL_ERROR "Line\n  ${LINE}\nin file\n  ${INCLCASCADE_RUN_VERSIONS_FILE}\ncontains ${LIST_LENGTH} items but must contain at least 3 items:\n  VERSION_NAME\tGIT_VERSION\tDEEXCITATION\t[other options]")
      endif(LIST_LENGTH LESS 3)
      list(GET LINE_AS_LIST 0 VERSION_NAME)
      list(GET LINE_AS_LIST 1 GIT_VERSION)
      list(GET LINE_AS_LIST 2 DEEXCITATION)

      # generate the inclxxrc file
      generate_inclxxrc_file(INCLXXRC_FILE ${VERSION_NAME})

      set(EXTRA_ARGS ${LINE_AS_LIST})
      list(REMOVE_AT EXTRA_ARGS 0)
      list(REMOVE_AT EXTRA_ARGS 0)
      list(REMOVE_AT EXTRA_ARGS 0)
      add_INCLCascade_version(${VERSION_NAME} ${GIT_VERSION} ${DEEXCITATION} ${INCLXXRC_FILE} ${EXTRA_ARGS})
      list(APPEND INCLCASCADE_THESE_VERSIONS ${VERSION_NAME})

    endif(NOT FIRST_CHARACTER STREQUAL "#")

  endforeach(LINE_UNSTRIPPED ${VERSIONS_FILE})

  # Process the RUN_FILE
  file(STRINGS "${INCLCASCADE_RUN_RUN_FILE}" RUN_FILE)

  # Loop over the lines in the versions config file and construct targets
  foreach(VERSION ${INCLCASCADE_THESE_VERSIONS})

    set(INCLCASCADE_RUNS_THIS_VERSION "")
    foreach(LINE ${RUN_FILE})
      string(REGEX MATCHALL "([^\ ]+)" LINE_AS_LIST "${LINE}")
      list(GET LINE_AS_LIST 0 RUN_NAME)
      list(REMOVE_AT LINE_AS_LIST 0) # the rest of the list contains the CLI options
      add_INCLCascade_run(${RUN_NAME} ${VERSION} ${LINE_AS_LIST})
      list(APPEND INCLCASCADE_RUNS_THIS_VERSION "${RUN_NAME}")
    endforeach(LINE ${RUN_FILE})

    if(INCLCASCADE_RUN_POSTPROCESS_BATCH)
      postprocess_batch("${INCLCASCADE_RUN_POSTPROCESS_BATCH}" "${INCLCASCADE_RUNS_THIS_VERSION}" "${VERSION}" "${INCLCASCADE_RUN_LABEL}")
    endif(INCLCASCADE_RUN_POSTPROCESS_BATCH)

  endforeach(VERSION ${INCLCASCADE_THESE_VERSIONS})

endfunction(INCLCascade_run)
